const express = require('express');
const body = require('body-parser');
const cookie = require('cookie-parser');

const session = require('express-session');

const mysql = require('mysql');
const connection = require('express-myconnection');
const path = require("path");


const app = express();

const { check } = require('express-validator');
const { validationResult } = require('express-validator');

//app.use(express.static('public'));
app.use(express.static(path.join(__dirname,'/public')));

//setting
app.use(express.static('public'));
app.set('view engine','ejs');
//app.set('views' , 'views');
app.set('views',path.join(__dirname,'/views'));

//middlewares

app.use(body.urlencoded({ extended: true}));
app.use(cookie());

app.use(session({
  secret: 'Passw0rd',
  resave: true,
  saveUninitialized: true
}));
app.use(connection(mysql,{
  host : 'database-ptt.ce9imirwsreh.ap-southeast-1.rds.amazonaws.com',
  user : 'pttoil',
  password : 'Passw0rdpttoil',
  port : 3306,
  database : 'pttoil',
  timezone: 'utc'
},'single'));


app.get('/',function(req,res){
  res.render('login',{session:req.session});
});
app.post('/',[check('username',"กรุณาระบุ Username!").not().isEmpty(),check('password',"กรุณาใส่ Password!").not().isEmpty()],function(req,res){
  const errors = validationResult(req);
  res.json
  if(!errors.isEmpty()){
    req.session.errors=errors;
    req.session.success=false;
    res.render('login',{session:req.session});
  }else{
    const username=req.body.username;
    const password=req.body.password;
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM login WHERE username = ? AND password = ? AND enable = 0 ',[username,password],(err,data) => {
        //res.json(data);
        conn.query('SELECT * FROM login WHERE username = ? AND password = ? AND enable = 1',[username,password],(err,data2) => {

        if(err){
          res.json(err);
        }else{
          if(data2.length>0){
            req.session.userid=data2[0].login_id;
            res.redirect('branch_center/');

          }else if  (data.length>0){
            req.session.userid=data[0].login_id;
              res.redirect('/oil');
          }else{
            req.session.test = "รหัสผ่านไม่ถูกต้อง";
            req.session.success = false;

            res.redirect('/');
            return;
          }
        }
      });
    });
  });
  }
});
app.get('/logout',function(req,res){
  req.session.destroy(function(error){
    res.redirect('/');
  });
});

const login_routes = require('./routes/login_routes');
app.use('/',login_routes);//login
const sender_routes = require('./routes/sender_routes');
app.use('/',sender_routes);//ผู้ส่ง
const branch_routes = require('./routes/branch_routes');
app.use('/',branch_routes);//สาขา
const branch_order_routes = require('./routes/branch_order_routes');
app.use('/',branch_order_routes);//ใบสั่งซื้อ
const oil_routes = require('./routes/oil_routes');
app.use('/',oil_routes);//รายการน้ำมัน
const branch_center_routes = require('./routes/branch_center_routes');
app.use('/',branch_center_routes);//รายการน้ำมัน
const price_routes = require('./routes/price_routes');
app.use('/',price_routes);//ราคาน้ำมัน
const bank_routes = require('./routes/bank_routes');
app.use('/',bank_routes);//รายการน้ำมัน
const balance_routes = require('./routes/balance_routes');
app.use('/',balance_routes);//ราคาน้ำมัน
const debt_routes = require('./routes/debt_routes');
app.use('/',debt_routes);//ยอดหนี้สะสม

//++++++++ New ++++++
const match_oil_branch_route = require('./routes/sale/match_oil_branch_route');
app.use('/',match_oil_branch_route);//
const oil_type_route = require('./routes/sale/oil_type_route');
app.use('/',oil_type_route);//
const sale_liter_route = require('./routes/sale/sale_liter_route');
app.use('/',sale_liter_route);//

app.listen('8081');
