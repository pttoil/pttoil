const express = require('express');
const router = express.Router();
const branch_controllers = require('../controllers/branch_controllers');
const validator = require('../controllers/validator');

router.get('/branch',branch_controllers.list);
router.get('/branch/new',branch_controllers.new);
router.post('/branch/add',validator.addbranch,branch_controllers.add);
router.get('/branch/delete/:id',branch_controllers.delete);
router.get('/branch/confirmdelete/:id',branch_controllers.confirmdelete);
router.get('/branch/edit/:id',branch_controllers.edit);
router.post('/branch/save/:id',validator.editbranch,branch_controllers.save);

module.exports = router;
