const express = require('express');
const router = express.Router();
const bank_controllers = require('../controllers/bank_controllers');
const validator = require('../controllers/validator');

router.get('/bank',bank_controllers.list);
router.get('/bank/new',bank_controllers.new);
router.post('/bank/add',bank_controllers.add);
router.get('/bank/delete/:id',bank_controllers.delete);
router.get('/bank/confirmdelete/:id',bank_controllers.confirmdelete);
router.get('/bank/edit/:id',bank_controllers.edit);
router.post('/bank/save/:id',bank_controllers.save);
module.exports = router;
