const express = require('express');
const router = express.Router();
const price_controllers = require('../controllers/price_controllers');
// const validator = require('../controllers/validator');

router.get('/price',price_controllers.list);
router.post('/price/check',price_controllers.check);
router.post('/price/different',price_controllers.different);
router.get('/price/differentlist',price_controllers.differentlist);
router.post('/price/totle',price_controllers.totle);
router.get('/price/totlelist',price_controllers.totlelist);
router.get('/price/new',price_controllers.new);
router.post('/price/add',price_controllers.add);
router.get('/price/delete/:id',price_controllers.delete);
router.get('/price/confirmdelete/:id',price_controllers.confirmdelete);
router.get('/price/edit/:id',price_controllers.edit);
router.post('/price/save/:id',price_controllers.save);

router.get('/profit',price_controllers.profit);
router.post('/checkprofit/check',price_controllers.checkprofit);

router.get('/sum',price_controllers.sum);
router.post('/checksum',price_controllers.checksum);

router.get('/test',price_controllers.test);
router.post('/retest',price_controllers.test);

module.exports = router;
