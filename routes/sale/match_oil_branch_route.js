const express = require('express');
const router = express.Router();
const match_oil_branch_controller = require('../../controllers/sale/match_oil_branch_controller');
// console.log("xx");
router.get('/match_oil_branch',match_oil_branch_controller.list);

router.get('/match_oil_branch/new/:id/:name',match_oil_branch_controller.new);
router.get('/match_oil_branch/add/:oil/:sale/:id/:name',match_oil_branch_controller.add);

// router.get('/match_oil_branch/edit/:id',match_oil_branch.edit);
// router.post('/match_oil_branch/save/:id',match_oil_branch.save);

// router.get('/match_oil_branch/delete/:id',match_oil_branch.delete);
// router.get('/match_oil_branch/confirmdelete/:id',match_oil_branch.confirmdelete);
module.exports = router;
