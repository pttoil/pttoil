const express = require('express');
const router = express.Router();
const oil_type_controllers = require('../../controllers/sale/oil_type_controllers');

router.get('/oil_type',oil_type_controllers.list);
router.get('/oil_type/new',oil_type_controllers.new);
router.post('/oil_type/add',oil_type_controllers.add);
router.get('/oil_type/delete/:id',oil_type_controllers.delete);
router.get('/oil_type/confirmdelete/:id',oil_type_controllers.confirmdelete);
router.get('/oil_type/edit/:id',oil_type_controllers.edit);
router.post('/oil_type/save/:id',oil_type_controllers.save);

module.exports = router;
