const express = require('express');
const router = express.Router();
const sale_liter_controllers = require('../../controllers/sale/sale_liter_controllers');

router.get('/sale_liter',sale_liter_controllers.list);
router.post('/sale_liter',sale_liter_controllers.list);
router.get('/sale_liter/back/:date',sale_liter_controllers.list);
router.get('/sale_liter/new/all/:date',sale_liter_controllers.new);
router.get('/sale_liter/new/branch/:brid/:date',sale_liter_controllers.new);
router.post('/sale_liter/add/:date',sale_liter_controllers.add);
router.get('/sale_liter/delete/:date',sale_liter_controllers.delete);
router.get('/sale_liter/delete/:brid/:date',sale_liter_controllers.delete);
router.get('/sale_liter/confirmdelete/:date',sale_liter_controllers.confirmdelete);
router.get('/sale_liter/confirmdelete/:brid/:date',sale_liter_controllers.confirmdelete);
router.get('/sale_liter/edit/:brid/:date',sale_liter_controllers.edit);
router.get('/sale_liter/edit/:date',sale_liter_controllers.edit);
router.post('/sale_liter/save/:date',sale_liter_controllers.save);
// day_conclude
router.get('/sale_liter/day_conclude',sale_liter_controllers.day_conclude);
router.post('/sale_liter/day_conclude',sale_liter_controllers.day_conclude);
router.get('/sale_liter/day_conclude/:date',sale_liter_controllers.day_conclude);
// average
router.get('/sale_liter/average/day_conclude/:date',sale_liter_controllers.day_conclude_average);
router.post('/sale_liter/average/day_conclude/:date',sale_liter_controllers.day_conclude_average);
// year_conclude
router.get('/sale_liter/year_conclude',sale_liter_controllers.year_conclude);
router.post('/sale_liter/year_conclude',sale_liter_controllers.year_conclude);

router.get('/sale_liter/year/report/:year',sale_liter_controllers.year_conclude_report);

module.exports = router;
