const express = require('express');
const router = express.Router();
const branch_order_controllers = require('../controllers/branch_order_controllers');
const validator = require('../controllers/validator');

router.get('/branch_order',branch_order_controllers.list);
router.get('/branch_order/new',branch_order_controllers.new);
router.post('/branch_order/add',validator.addbranch_order,branch_order_controllers.add);
router.get('/branch_order/delete/:id',branch_order_controllers.delete);
router.get('/branch_order/confirmdelete/:id',branch_order_controllers.confirmdelete);
router.get('/branch_order/edit/:id',branch_order_controllers.edit);
router.post('/branch_order/save/:id',validator.editbranch_order,branch_order_controllers.save);
module.exports = router;
