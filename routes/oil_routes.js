const express = require('express');
const router = express.Router();
const oil_controllers = require('../controllers/oil_controllers');
//const validator = require('../controllers/validator');

router.get('/oil',oil_controllers.list)
router.post('/oil_date/check',oil_controllers.check);

router.get('/oil/new',oil_controllers.new);
router.post('/oil/add',oil_controllers.add);

router.get('/oil/edit/:id',oil_controllers.edit);
router.post('/oil/save/:id',oil_controllers.save);

router.get('/oil/delete/:id',oil_controllers.delete);
router.get('/oil/confirmdelete/:id',oil_controllers.confirmdelete);

module.exports = router;
