const express = require('express');
const router = express.Router();
const balance_controllers = require('../controllers/balance_controllers');
const validator = require('../controllers/validator');

router.get('/balance',balance_controllers.list);
router.get('/balance/new',balance_controllers.new);
router.post('/balance/add',balance_controllers.add);
router.get('/balance/delete/:id',balance_controllers.delete);
router.get('/balance/confirmdelete/:id',balance_controllers.confirmdelete);
router.get('/balance/edit/:id',balance_controllers.edit);
router.post('/balance/save/:id',balance_controllers.save);
router.post('/balance/check',balance_controllers.check);
module.exports = router;
