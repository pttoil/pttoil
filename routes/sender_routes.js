const express = require('express');
const router = express.Router();
const sender_controllers = require('../controllers/sender_controllers');
const validator = require('../controllers/validator');

router.get('/sender',sender_controllers.list);
router.get('/sender/new',sender_controllers.new);
router.post('/sender/add',validator.addsender,sender_controllers.add);
router.get('/sender/delete/:id',sender_controllers.delete);
router.get('/sender/confirmdelete/:id',sender_controllers.confirmdelete);
router.get('/sender/edit/:id',sender_controllers.edit);
router.post('/sender/save/:id',validator.editsender,sender_controllers.save);
module.exports = router;
