const express = require('express');
const router = express.Router();
const balance_controllers = require('../controllers/debt_controllers');
const validator = require('../controllers/validator');

router.get('/debt',balance_controllers.list);
router.get('/debt/new',balance_controllers.new);
router.post('/debt/add',balance_controllers.add);
router.get('/debt/delete/:id',balance_controllers.delete);
router.get('/debt/confirmdelete/:id',balance_controllers.confirmdelete);
router.get('/debt/edit/:id',balance_controllers.edit);
router.post('/debt/save/:id',balance_controllers.save);
router.post('/debt/check',balance_controllers.check);
module.exports = router;
