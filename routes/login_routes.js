const express = require('express');
const router = express.Router();
const login_controllers = require('../controllers/login_controllers');
const validator = require('../controllers/validator');

router.get('/login',login_controllers.list);
router.get('/login/new',login_controllers.new);
router.post('/login/add',validator.addlogin,login_controllers.add);
router.get('/login/delete/:id',login_controllers.delete);
router.get('/login/confirmdelete/:id',login_controllers.confirmdelete);
router.get('/login/edit/:id',login_controllers.edit);
router.post('/login/save/:id',validator.editlogin,login_controllers.save);
module.exports = router;
