const express = require('express');
const router = express.Router();
const branch_center_controllers = require('../controllers/branch_center_controllers');
const validator = require('../controllers/validator');

router.get('/branch_center',branch_center_controllers.list);
router.post('/branch_center_date/check',branch_center_controllers.check);

router.get('/branch_center/new',branch_center_controllers.new);
router.post('/branch_center/add',validator.addbranch_center,branch_center_controllers.add);

router.get('/branch_center/edit/:id',branch_center_controllers.edit);
router.post('/branch_center/save/:id',branch_center_controllers.save);

router.get('/branch_center/delete/:id',branch_center_controllers.delete);
router.get('/branch_center/confirmdelete/:id',branch_center_controllers.confirmdelete);

//add oil user
router.get('/oil_b/new',branch_center_controllers.newo);
router.post('/oil_b/add',branch_center_controllers.addo);

router.get('/oil_b/edit/:id',branch_center_controllers.edito);
router.post('/oil_b/save/:id',branch_center_controllers.saveo);

router.get('/oil_b/delete/:id',branch_center_controllers.deleteo);
router.get('/oil_b/confirmdelete/:id',branch_center_controllers.confirmdeleteo);
module.exports = router;
