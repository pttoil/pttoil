const { check } = require('express-validator');

exports.addsender = [check('sender_name',"กรุณากรอกข้อมูลให้ถูกต้อง !!").not().isEmpty()];
exports.editsender = [check('sender_name',"กรุณาแก้ไขข้อมูลให้ถูกต้อง !!").not().isEmpty()];

exports.addlogin = [check('username',"กรุณากรอกชื่อผู้ใช้ให้ถูกต้อง !!").not().isEmpty(),check('password',"กรุณากรอกพาสเวิร์ดให้ถูกต้อง !!").not().isEmpty(),check('name',"กรุณากรอกข้อมูลชื่อให้ถูกต้อง !!").not().isEmpty(),check('lastname',"กรุณากรอกข้อมูลนามสกุลให้ถูกต้อง !!").not().isEmpty()];
exports.editlogin = [check('username',"กรุณาแก้ไขชื่อผู้ใช้ให้ถูกต้อง !!").not().isEmpty(),check('password',"กรุณาแก้ไขพาสเวิร์ดให้ถูกต้อง !!").not().isEmpty(),check('name',"กรุณาแก้ไขข้อมูลชื่อให้ถูกต้อง !!").not().isEmpty(),check('lastname',"กรุณาแก้ไขข้อมูลนามสกุลให้ถูกต้อง !!").not().isEmpty()];

exports.addbranch = [check('branch_name',"กรุณากรอกชื่อสาขาให้ถูกต้อง !!").not().isEmpty(),check('address',"กรุณากรอกที่อยู่ให้ถูกต้อง !!").not().isEmpty(),check('telephone',"กรุณากรอกเบอร์โทรศัพท์ให้ถูกต้อง !!").not().isEmpty()];
exports.editbranch= [check('branch_name',"กรุณาแก้ไขชื่อสาขาให้ถูกต้อง !!").not().isEmpty(),check('address',"กรุณาแก้ไขที่อยู่ให้ถูกต้อง !!").not().isEmpty(),check('telephone',"กรุณาแก้ไขเบอร์โทรศัพท์ให้ถูกต้อง !!").not().isEmpty()];

exports.addbranch_order = [check('order_name',"กรุณากรอกข้อมูลให้ถูกต้อง !!").not().isEmpty()];
exports.editbranch_order = [check('order_name',"กรุณาแก้ไขข้อมูลให้ถูกต้อง !!").not().isEmpty()];

exports.addbranch_center = [check('oil_id',"กรุณาเลือกสาขาให้ถูกต้อง !!").isNumeric(),check('sender_id',"กรุณาเลือกผู้ส่งให้ถูกต้อง !!").isNumeric(),check('order_id',"กรุณาเลือกใบสั่งซื้อ (ตั๋ว) ให้ถูกต้อง !!").isNumeric()];
exports.editbranch_center = [check('oil_id',"กรุณาเลือกสาขาให้ถูกต้อง !!").isNumeric(),check('sender_id',"กรุณาเลือกผู้ส่งให้ถูกต้อง !!").isNumeric(),check('order_id',"กรุณาเลือกใบสั่งซื้อ (ตั๋ว) ให้ถูกต้อง !!").isNumeric()];

exports.addoil_b = [check('branch_id',"กรุณาเลือกสาขาให้ถูกต้อง !!").isNumeric()];
exports.editoil_b = [check('branch_id',"กรุณาเลือกสาขาให้ถูกต้อง !!").isNumeric()];

exports.addoil = [check('branch_id',"กรุณาเลือกสาขาให้ถูกต้อง !!").isNumeric()];
exports.editoil = [check('branch_id',"กรุณาเลือกสาขาให้ถูกต้อง !!").isNumeric()];
