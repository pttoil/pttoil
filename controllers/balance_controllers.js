const controller = {};
const { validationResult } = require('express-validator');

controller.list = (req,res) => {
  const data = req.body;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) =>{
      conn.query('SELECT bl.id,bl.bank_id,bl.balance,b.name,date_format(bl.date,"%d/%m/%Y")as date FROM balance as bl LEFT JOIN bank as b ON b.id=bl.bank_id  ORDER BY bl.date DESC',(err,balance_list)=>{
        if(err){
          res.json(err);
        }

        res.render('balance_list',{
          data:balance_list,session: req.session
        });
      });
    });
  }
};

controller.new = (req,res) => {
  const data = null ;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM bank ',(err,balance_new)=>{
        res.render('balance_new',{
          data:balance_new , session: req.session
        });
      });
    });
  }
};

controller.add = (req,res) => {
  const data = req.body;
  const errors = validationResult(req);
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    if(!errors.isEmpty()){
      req.session.errors = errors;
      req.session.success = false;
      res.redirect('/balance/new');
    }else{
      req.session.success = true;
      req.session.topic = "เพิ่มข้อมูลสำเร็จ";
      req.getConnection((err,conn) =>{
        conn.query('INSERT INTO balance set ?',[data],(err,balance_add) =>{
          console.log(balance_add);
          res.redirect('/balance');
        });
      });
    }
  }
};

controller.delete = (req,res) => {
  const {id} = req.params ;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) => {
      conn.query('SELECT bl.id,bl.bank_id,bl.balance,b.name,date_format(bl.date,"%d/%m/%Y")as date FROM balance as bl LEFT JOIN bank as b ON b.id=bl.bank_id where bl.id = ?',[id],(err,balance_delete)=>{
        if (err){
          res.json(err);
        }
        res.render('balance_delete',{
          data:balance_delete ,session: req.session
        });
      });
    });
  }
};

controller.confirmdelete = (req,res) => {
  const errors = validationResult(req);
  const {id}=req.params;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    if(!errors.isEmpty()){
      req.session.errors = errors;
      req.session.success = false;
      res.redirect('/balance/delete'+id);
    }else{
      req.getConnection((err,conn) =>{
        conn.query('DELETE FROM balance WHERE id=?',[id],(err,balance_confirmdelete)=>{
          if (err){

            req.session.test = "ไม่สามารถลบได้";
            req.session.success = false;

            res.redirect('/balance');
            return;
          }else {
            req.session.success = true;
            req.session.topic = "ลบข้อมูลสำเร็จ";

          }
          console.log(balance_confirmdelete);
          res.redirect('/balance/');
        });
      });
    }
  }
};

controller.edit = (req,res) => {
  const data = req.body;
  const {id} = req.params;

  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) => {
      conn.query('SELECT bl.id,bl.bank_id,bl.balance,b.name,bl.date as date FROM balance as bl LEFT JOIN bank as b ON b.id=bl.bank_id WHERE bl.id =?',[id],(err,balance_edit)=>{
        conn.query('SELECT * FROM bank' ,(err,data1)=>{
          res.render('balance_edit',{
            data:balance_edit,data1:data1  , session: req.session
          });
        });
      });
    });
  }
};

controller.save = (req,res) => {
  const {id} = req.params ;
  const data = req.body;
  //res.json(data);

  const errors = validationResult(req) ;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    if (!errors.isEmpty()) {
      req.session.errors = errors;
      req.session.success = false;
      res.redirect('/balance/edit/'+id)
    } else {
      req.session.success = true;
      req.session.topic = "แก้ไขข้อมูลสำเร็จ";
      req.getConnection((err,conn) => {
        conn.query('UPDATE balance SET ? WHERE id = ? ',[data,id],(err,balance_save)=>{
          if (err){
            res.json(err);
          }
          console.log(data);
          res.redirect('/balance/');
        });
      });
    }
  }
}

controller.check = (req,res) => {
  const {date}=req.body;
  const {id} = req.params;
  //res.json(date);
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) =>{
      conn.query('SELECT bl.id,bl.bank_id,bl.balance,b.name,date_format(bl.date,"%d/%m/%Y")as date FROM balance as bl LEFT JOIN bank as b ON b.id=bl.bank_id WHERE date_format(bl.date,"%Y-%m-%d")<=? ORDER BY bl.date DESC LIMIT 3',[date],(err,data)=>{
        //  res.json(data);
        if(err){
          res.json(err);
        }
        res.render('balance_check',{
          data:data,session:req.session

        });
      });
    });
  }
};

module.exports = controller;
