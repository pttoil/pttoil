const controller = {};

controller.list = (req,res) => {
  const data = req.body;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM oil_type',(err,oil_type_list)=>{
        if(err){
          res.json(err);
        }
        res.render('sale/oil_type/oil_type_list',{
          data:oil_type_list,session: req.session
        });
      });
    });
  }
};

controller.new = (req,res) => {
  const data = null ;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) => {
      res.render('sale/oil_type/oil_type_new',{
        session: req.session
      });
    });
  }
};

controller.add = (req,res) => {
  const data = req.body;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) =>{
        for (var i = 0; i < data.name.length; i++) {
          if (i > 0) {
            conn.query('INSERT INTO oil_type set name = ?, color = ?',[data.name[i],data.color[i]],(err,oil_type_add) =>{
              if (err) {
                res.json(err);
              }else {
                req.session.success = true;
                req.session.topic = "เพิ่มข้อมูลสำเร็จ";
              }
              console.log(oil_type_add);
            });
          }
        }
      res.redirect('/oil_type');
    });
  }
};

controller.delete = (req,res) => {
  const {id} = req.params ;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM oil_type where id = ?',[id],(err,oil_type_delete)=>{
        if (err){
          res.json(err);
        }
        res.render('sale/oil_type/oil_type_delete',{
          data:oil_type_delete ,session: req.session
        });
      });
    });
  }
};

controller.confirmdelete = (req,res) => {
  const {id}=req.params;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) =>{
      conn.query('DELETE FROM oil_type WHERE id = ?',[id],(err,oil_type_confirmdelete)=>{
        if (err){
          req.session.test = "ไม่สามารถลบได้";
          req.session.success = false;
          res.redirect('/oil_type');
          return;
        }else {
          req.session.success = true;
          req.session.topic = "ลบข้อมูลสำเร็จ";
        }
        res.redirect('/oil_type');
      });
    });
  }
};

controller.edit = (req,res) => {
  const data = req.body;
  const {id} = req.params;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM oil_type WHERE id = ?',[id],(err,oil_type_edit)=>{
        res.render('sale/oil_type/oil_type_edit',{
          data:oil_type_edit,session: req.session
        });
      });
    });
  }
};

controller.save = (req,res) => {
  const {id} = req.params ;
  const data = req.body;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) => {
      conn.query('UPDATE oil_type SET ? WHERE id = ? ',[data,id],(err,oil_type_save)=>{
        if (err){
          req.session.test = "ไม่สามารถแก้ไขได้";
          req.session.success = false;
          res.redirect('/oil_type/edit/'+id);
        }else {
          req.session.success = true;
          req.session.topic = "แก้ไขข้อมูลสำเร็จ";
          res.redirect('/oil_type');
        }
      });
    });
  }
}

module.exports = controller;
