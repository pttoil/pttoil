const controller = {};

controller.list = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT match_oil_branch.*,oil_type.name as oiltype,branch.branch_name FROM match_oil_branch JOIN oil_type on match_oil_branch.oil_type_id = oil_type.id JOIN branch ON match_oil_branch.branch_id = branch.branch_id', (err, match_oil_branch) => {
                conn.query('SELECT * FROM branch', (err, branch) => {
                    res.render('sale/match_oil_branch/match_oil_branch_list', {
                        branch: branch,
                        match_oil_branch: match_oil_branch,
                        session: req.session
                    });
                });
            });
        });
    }
};

controller.new = (req, res) => {
    var url = req.params;
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT match_oil_branch.*,oil_type.name as oiltype,branch.branch_name FROM match_oil_branch JOIN oil_type on match_oil_branch.oil_type_id = oil_type.id JOIN branch ON match_oil_branch.branch_id = branch.branch_id where match_oil_branch.branch_id = ?', [url.id], (err, match_oil_branch) => {
                conn.query('SELECT * FROM oil_type ', (err, oil_type) => {
                    res.render('sale/match_oil_branch/match_oil_branch_new', {
                        url: url,
                        match_oil_branch: match_oil_branch,
                        oil_type: oil_type,
                        session: req.session
                    });
                });
            });
        });
    }
};

controller.add = (req, res) => {
    var url = req.params;
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        // req.session.success = true;
        // req.session.topic = "เพิ่มข้อมูลสำเร็จ";
        req.getConnection((err, conn) => {
            if (url.sale == 1) {
                conn.query('SELECT * FROM match_oil_branch where match_oil_branch.branch_id = ? AND match_oil_branch.oil_type_id =?', [url.id, url.oil], (err, match_oil_branch) => {
                    if (match_oil_branch[0]) {
                        conn.query('UPDATE match_oil_branch SET cancle = 1 WHERE id = ? ', [match_oil_branch[0].id], (err, ok) => {
                          if (err) {
                            res.json(err);
                          }
                        res.redirect('/match_oil_branch/new/'+url.id+'/'+url.name);
                        });
                    } else {
                        conn.query('INSERT INTO match_oil_branch set oil_type_id = ?,branch_id = ?,cancle = 1', [url.oil,url.id], (err, ok) => {
                            if (err) {
                              res.json(err);
                            }
                            res.redirect('/match_oil_branch/new/'+url.id+'/'+url.name);
                        });
                    }
                });
            } else {
                conn.query('SELECT * FROM sale_liter where sale_liter.branch_id = ? AND sale_liter.oil_type_id =  ?', [url.id, url.oil], (err, sale_liter) => {
                    if (sale_liter[0]) {
                        conn.query('UPDATE match_oil_branch SET cancle = 0 WHERE match_oil_branch.branch_id = ? AND match_oil_branch.oil_type_id =?', [url.id, url.oil], (err, ok) => {
                            if (err) {
                              res.json(err);
                            }
                            res.redirect('/match_oil_branch/new/'+url.id+'/'+url.name);
                        });
                    } else {
                        conn.query('DELETE FROM match_oil_branch WHERE match_oil_branch.branch_id = ? AND match_oil_branch.oil_type_id =?', [url.id, url.oil], (err, ok) => {
                            if (err) {
                              res.json(err);
                            }
                            res.redirect('/match_oil_branch/new/'+url.id+'/'+url.name);
                        });
                    }
                });
            }

        });
    }
};

module.exports = controller;
