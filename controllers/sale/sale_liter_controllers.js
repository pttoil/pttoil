const controller = {};
const request = require('request');

controller.list = (req,res) => {
  var url = req.params;
  var d = new Date();
  d.setDate(d.getDate()-1);
  var day = d.getDate();
  if (day < 10) {
    day = "0"+day;
  }
  var month = d.getMonth()+1;
  if (month < 10) {
    month = "0"+month;
  }
  var year = d.getFullYear();
  var fullDate = year+'-'+month+'-'+day;
  const data = req.body;
  if (data.fullDate) {
    fullDate = data.fullDate;
  }
  if (url.date) {
    fullDate = url.date;
  }
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) =>{
      conn.query('select * from branch where in_out = 1',(err,branch)=>{
        conn.query('select * from match_oil_branch as mb join oil_type on oil_type_id = oil_type.id join branch on branch.branch_id = mb.branch_id where in_out = 1 and cancle = 1 order by mb.branch_id,oil_type.id',(err,oil_type1)=>{
          conn.query('select b.*,ot.* from match_oil_branch as mb join oil_type as ot on mb.oil_type_id = ot.id join branch as b on b.branch_id = mb.branch_id join sale_liter as sl on (sl.oil_type_id = ot.id and sl.branch_id = b.branch_id) where in_out = 1 and date = ? order by mb.branch_id,ot.id',[fullDate],(err,oil_type2)=>{
            conn.query('SELECT * FROM sale_liter where date = ?',[fullDate],(err,sale_liter_list)=>{
              if(err){
                res.json(err);
              }
              res.render('sale/sale_liter/sale_liter_list',{
                branch,oil_type1,oil_type2,data:sale_liter_list,fullDate,session: req.session
              });
            });
          });
        });
      });
    });
  }
};

controller.new = (req,res) => {
  const data = null ;
  var url = req.params;
  var fullDate = url.date;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) => {
      var branch_sql, oil_type_sql;
      if (url.brid) {
        branch_sql = 'select * from branch where branch_id = "'+url.brid+'" and in_out = 1';
        oil_type_sql = 'select * from match_oil_branch as mb join oil_type on oil_type_id = oil_type.id join branch on branch.branch_id = mb.branch_id where branch.branch_id = "'+url.brid+'" and in_out = 1 and cancle = 1 order by mb.branch_id,oil_type.id';
      }else {
        branch_sql = 'select * from branch where in_out = 1'
        oil_type_sql = 'select * from match_oil_branch as mb join oil_type on oil_type_id = oil_type.id join branch on branch.branch_id = mb.branch_id where in_out = 1 and cancle = 1 order by mb.branch_id,oil_type.id';
      }
      conn.query(branch_sql,(err,branch)=>{
        conn.query(oil_type_sql,(err,oil_type)=>{
          res.render('sale/sale_liter/sale_liter_new',{
            branch,oil_type,fullDate,session: req.session
          });
        });
      });
    });
  }
};

controller.add = (req,res) => {
  var url = req.params;
  const data = req.body;
  var fullDate = url.date
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {

    req.getConnection((err,conn) =>{
      for (var i = 0; i < data.branch_id.length; i++) {
        conn.query('INSERT INTO sale_liter set oil_type_id = ?, branch_id = ?, date = ?, liter = ?',[data.oil_type_id[i],data.branch_id[i],data.date[i],data.liter[i]],(err,sale_liter_add) =>{
          if (err) {
            res.json(err);
          }else {
            req.session.success = true;
            req.session.topic = "เพิ่มข้อมูลสำเร็จ";
          }
        });
      }
      res.redirect('/sale_liter/back/'+fullDate);
    });
  }
};

controller.delete = (req,res) => {
  var url = req.params ;
  var fullDate = url.date;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) => {
      var branch_sql, oil_type_sql, sale_liter_sql;
      if (url.brid) {
        branch_sql = 'select * from branch where branch_id = "'+url.brid+'" and in_out = 1';
        oil_type_sql = 'select b.*,ot.* from match_oil_branch as mb join oil_type as ot on mb.oil_type_id = ot.id join branch as b on b.branch_id = mb.branch_id join sale_liter as sl on (sl.oil_type_id = ot.id and sl.branch_id = b.branch_id) where sl.branch_id = '+url.brid+' and date = "'+url.date+'" order by mb.branch_id,ot.id';
        sale_liter_sql = 'SELECT * FROM sale_liter WHERE branch_id = "'+url.brid+'" and date = "'+url.date+'"';
      }else {
        branch_sql = 'select * from branch where in_out = 1'
        oil_type_sql = 'select b.*,ot.* from match_oil_branch as mb join oil_type as ot on mb.oil_type_id = ot.id join branch as b on b.branch_id = mb.branch_id join sale_liter as sl on (sl.oil_type_id = ot.id and sl.branch_id = b.branch_id) where date = "'+url.date+'" order by mb.branch_id,ot.id';
        sale_liter_sql = 'SELECT * FROM sale_liter WHERE date = "'+url.date+'"';
      }
      conn.query(branch_sql,(err,branch)=>{
        conn.query(oil_type_sql,(err,oil_type)=>{
          conn.query(sale_liter_sql,(err,sale_liter_delete)=>{
            if (err){
              res.json(err);
            }
            res.render('sale/sale_liter/sale_liter_delete',{
              branch,oil_type,data:sale_liter_delete,fullDate,session: req.session
            });
          });
        });
      });
    });
  }
};

controller.confirmdelete = (req,res) => {
  const url = req.params;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) =>{
      var sale_liter_sql;
      if (url.brid) {
        sale_liter_sql = 'DELETE FROM sale_liter WHERE branch_id = "'+url.brid+'" and date = "'+url.date+'"';
      } else {
        sale_liter_sql = 'DELETE FROM sale_liter WHERE date = "'+url.date+'"';
      }
      conn.query(sale_liter_sql,(err,sale_liter_confirmdelete)=>{
        if (err){
          req.session.test = "ไม่สามารถลบได้";
          req.session.success = false;
          res.redirect('/sale_liter');
          return;
        }else {
          req.session.success = true;
          req.session.topic = "ลบข้อมูลสำเร็จ";
        }
        if (url.brid) {
          res.redirect('/sale_liter/back/'+url.date);
        }else {
          res.redirect('/sale_liter');
        }
      });
    });
  }
};

controller.edit = (req,res) => {
  const data = req.body;
  var url = req.params;
  var fullDate = url.date;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) => {
      var branch_sql, oil_type_sql, sale_liter_sql;
      if (url.brid) {
        branch_sql = 'select * from branch where branch_id = "'+url.brid+'" and in_out = 1';
        oil_type_sql = 'select b.*,ot.* from match_oil_branch as mb join oil_type as ot on mb.oil_type_id = ot.id join branch as b on b.branch_id = mb.branch_id join sale_liter as sl on (sl.oil_type_id = ot.id and sl.branch_id = b.branch_id) where sl.branch_id = '+url.brid+' and date = "'+url.date+'" order by mb.branch_id,ot.id';
        sale_liter_sql = 'SELECT * FROM sale_liter WHERE branch_id = "'+url.brid+'" and date = "'+url.date+'"';
      }else {
        branch_sql = 'select * from branch where in_out = 1'
        oil_type_sql = 'select b.*,ot.* from match_oil_branch as mb join oil_type as ot on mb.oil_type_id = ot.id join branch as b on b.branch_id = mb.branch_id join sale_liter as sl on (sl.oil_type_id = ot.id and sl.branch_id = b.branch_id) where date = "'+url.date+'" order by mb.branch_id,ot.id';
        sale_liter_sql = 'SELECT * FROM sale_liter WHERE date = "'+url.date+'"';
      }
      conn.query(branch_sql,(err,branch)=>{
        conn.query(oil_type_sql,(err,oil_type)=>{
          conn.query(sale_liter_sql,(err,sale_liter_edit)=>{
            res.render('sale/sale_liter/sale_liter_edit',{
              branch,oil_type,data:sale_liter_edit,fullDate,session: req.session
            });
          });
        });
      });
    });
  }
};

controller.save = (req,res) => {
  var url = req.params;
  const data = req.body;
  var fullDate = url.date;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) => {
      for (var i = 0; i < data.liter.length; i++) {
        conn.query('UPDATE sale_liter SET branch_id = ?, oil_type_id = ?, date = ?, liter = ? WHERE id = ? ',[data.branch_id[i],data.oil_type_id[i],data.date[i],data.liter[i],data.id[i]],(err,sale_liter_save)=>{
          if (err){
            req.session.success = false;
            req.session.topic = "ไม่สามารถแก้ไขข้อมูลได้";
            res.redirect('/sale_liter/back/'+fullDate);
          }else {
            req.session.success = true;
            req.session.topic = "แก้ไขข้อมูลสำเร็จ";
          }
        });
      }
      res.redirect('/sale_liter/back/'+fullDate);
    });
  }
}

controller.day_conclude = (req,res) => {
  var url = req.params;
  const data = req.body;
  var d = new Date();
  var day = d.getDate()-1;
  if (day < 10) {
    day = "0"+day;
  }
  var month = d.getMonth()+1;
  if (month < 10) {
    month = "0"+month;
  }
  var year = d.getFullYear();
  var fullDate = year+'-'+month+'-'+day;
  if (url.date) {
    fullDate = url.date;
  }
  if (data.fullDate) {
    fullDate = data.fullDate;
  }
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) =>{
      conn.query('select * from branch where in_out = 1',(err,branch)=>{
        conn.query('select ot.* from oil_type as ot join match_oil_branch as mb on mb.oil_type_id = ot.id join branch as b on mb.branch_id = b.branch_id join sale_liter as sl on (sl.oil_type_id = mb.oil_type_id and sl.branch_id = mb.branch_id) where sl.date = ? group by ot.id',[fullDate],(err,oil_type)=>{
          conn.query('SELECT * FROM sale_liter where date = ?',[fullDate],(err,sale_liter_list)=>{
            conn.query('select sum(liter) as sum from sale_liter where date = ? group by branch_id',[fullDate],(err,sum)=>{
              if(err){
                res.json(err);
              }
              res.render('sale/sale_liter/day_conclude',{
                branch,oil_type,data:sale_liter_list,sum,fullDate,session: req.session
              });
            });
          });
        });
      });
    });
  }
};

controller.day_conclude_average = (req,res) => {
  var url = req.params;
  const data = req.body;
  var d = new Date(url.date);
  var fullDate = url.date;
  var fullYear = d.getFullYear();
  if (data.fullYear) {
    fullYear = data.fullYear;
  }
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) =>{
      conn.query('select * from branch where in_out = 1',(err,branch)=>{
        conn.query('select oil_type.* from oil_type join match_oil_branch on match_oil_branch.oil_type_id = oil_type.id group by oil_type.id',(err,oil_type)=>{
          var keepBranch = [];
          for (var i = 0; i < oil_type.length; i++) {
            conn.query('select * from match_oil_branch join branch on match_oil_branch.branch_id = branch.branch_id where oil_type_id = ? and in_out = 1',[oil_type[i].id],(err,oil_branch)=>{
              keepBranch.push(oil_branch);
            });
          }
          conn.query('select sum(liter) as sum,count(date) as count, DATE_FORMAT(date,"%m") as m, oil_type_id, branch_id from sale_liter where DATE_FORMAT(date,"%Y") = ? group by oil_type_id,branch_id,DATE_FORMAT(date,"%m")',[fullYear],(err,sum)=>{
            conn.query('select sum(liter) as sum,count(date) as count, DATE_FORMAT(date,"%m") as m, oil_type_id, branch_id from sale_liter where DATE_FORMAT(date,"%Y") = ? group by DATE_FORMAT(date,"%m")',[fullYear],(err,total)=>{
              conn.query('select DATE_FORMAT(date, "%Y") as year from sale_liter group by DATE_FORMAT(date, "%Y")',[fullYear],(err,year)=>{
                if(err){
                  res.json(err);
                }
                res.render('sale/sale_liter/day_average',{
                  branch,oil_type,keepBranch,sum,total,fullDate,fullYear,year,session: req.session
                });
              });
            });
          });
        });
      });
    });
  }
};

controller.year_conclude = (req,res) => {
  const data = req.body;
  var d = new Date();
  var month = d.getMonth()+1;
  if (month < 10) {
    month = "0"+month;
  }
  var fullYear = d.getFullYear();
  if (data.fullYear) {
    fullYear = data.fullYear;
  }
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) =>{
      conn.query('select * from branch where in_out = 1',(err,branch)=>{
        conn.query('SELECT DATE_FORMAT(date, "%m") as month, branch_id, sum(liter) as sum FROM sale_liter where DATE_FORMAT(date, "%Y") = ? group by branch_id,DATE_FORMAT(date,"%m-%Y")',[fullYear],(err,sale_liter_list)=>{
          conn.query('select sum(liter) as sum from sale_liter where DATE_FORMAT(date, "%Y") = ? group by branch_id',[fullYear],(err,sum)=>{
            conn.query('select DATE_FORMAT(date, "%Y") as year from sale_liter group by DATE_FORMAT(date, "%Y")',[fullYear],(err,year)=>{
              if(err){
                res.json(err);
              }
              res.render('sale/sale_liter/year_conclude',{
                branch,data:sale_liter_list,sum,fullYear,year,session: req.session
              });
            });
          });
        });
      });
    });
  }
};

controller.year_conclude_report = (req,res) => {
  var url = req.params;
  var fullYear = url.year;
  var thaiYear = (+fullYear)+543;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) =>{
      conn.query('select * from branch where in_out = 1',(err,branch)=>{
        conn.query('SELECT DATE_FORMAT(date, "%m") as month, branch_id, sum(liter) as sum FROM sale_liter where DATE_FORMAT(date, "%Y") = ? group by branch_id,DATE_FORMAT(date,"%m-%Y")',[fullYear],(err,sale_liter)=>{
          conn.query('select sum(liter) as sum from sale_liter where DATE_FORMAT(date, "%Y") = ? group by branch_id',[fullYear],(err,sum)=>{
            conn.query('select DATE_FORMAT(date, "%Y") as year from sale_liter group by DATE_FORMAT(date, "%Y")',[fullYear],(err,year)=>{
              var cols = branch.length;
              if(err){
                res.json(err);
              }
              var m = ["มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฏาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"];
              var count = 0,c,keeps = [];for (var j = 0; j < m.length; j++) {var sumY = 0,keepb = [];
                for (var i = 0; i < branch.length; i++) {
                  for (var k = 0; k < sale_liter.length; k++) {
                    if (branch[i].branch_id == sale_liter[k].branch_id && j == (parseInt(sale_liter[k].month)-1)) {
                      keepb.push(sale_liter[k].sum.toFixed().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
                      sumY += sale_liter[k].sum;
                      if (c != sale_liter[k].month) {
                        c = sale_liter[k].month
                        count++
                      }
                    }
                  }
                }
                if (keepb.length == 0) {
                  for (var i = 0; i < branch.length; i++) {
                    keepb.push("");
                  }
                }
                if (sumY == 0) {
                  sumY = '';
                }else {
                  sumY = sumY.toFixed().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                }
                var sum_names = {};
                for(var i = 0; i< keepb.length; i++){
                  sum_names['sum_'+i] = keepb[i];
                }
                keeps.push({no:j+1,month:m[j],...sum_names,sumY:sumY});
              }
              var keepSum = [], totalSum = 0, average = [], totalAverage = 0;
              for (var i = 0; i < sum.length; i++) {
                totalSum += sum[i].sum;
                totalAverage += (sum[i].sum/count);
                average.push({avr:(sum[i].sum/count).toFixed().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')});
                keepSum.push({sum:sum[i].sum.toFixed().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')});
              }
              totalSum = totalSum.toFixed().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
              totalAverage = totalAverage.toFixed().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
              // console.log(keeps);
              var data = {
                template:{'shortid':'pbm_71que2'},
                data:{
                  thaiYear,branch,keeps,cols,keepSum,totalSum,average,totalAverage
                }
              }
              var options = {
                url: 'https://denhah.jsreportonline.net/api/report',
                method: 'POST',
                headers: {
                  'Authorization': 'Basic bm9ucHR0ZGVuaGFoQGdtYWlsLmNvbToxMjM0',
                  'Content-Type': 'application/json',
                },
                json: data
              }

              // var options = {
              //   url: 'http://127.0.0.1:5488/api/report',
              //   method: 'POST',
              //   // headers: {
              //   //     'Authorization': 'Basic YWRtaW46UGFzc3cwcmQ=',
              //   //     'Content-Type': 'application/json',
              //   //   },
              //   json: data
              // }
              request(options).pipe(res);
            });
          });
        });
      });
    });
  }
};

module.exports = controller;
