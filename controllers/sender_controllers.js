const controller = {};
const { validationResult } = require('express-validator');

controller.list = (req,res) => {
  const data = req.body;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM sender ',(err,sender_list)=>{
        if(err){
          res.json(err);
        }

        res.render('sender_list',{
          data:sender_list,session: req.session
        });
      });
    });
  }
};

controller.new = (req,res) => {
  const data = null ;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM sender ',(err,sender_new)=>{
        res.render('sender_new',{
          data2:sender_new , session: req.session
        });
      });
    });
  }
};

controller.add = (req,res) => {
  const data = req.body;
  const errors = validationResult(req);
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    if(!errors.isEmpty()){
      req.session.errors = errors;
      req.session.success = false;
      res.redirect('/sender/new');
    }else{
      req.session.success = true;
      req.session.topic = "เพิ่มข้อมูลสำเร็จ";
      req.getConnection((err,conn) =>{
        conn.query('INSERT INTO sender set ?',[data],(err,sender_add) =>{
          console.log(sender_add);
          res.redirect('/sender');
        });
      });
    }
  }
};

controller.delete = (req,res) => {
  const {id} = req.params ;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM sender where sender_id = ?',[id],(err,sender_delete)=>{
        if (err){
          res.json(err);
        }
        res.render('sender_delete',{
          data1:sender_delete ,session: req.session
        });
      });
    });
  }
};

controller.confirmdelete = (req,res) => {
  const errors = validationResult(req);
  const {id}=req.params;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    if(!errors.isEmpty()){
      req.session.errors = errors;
      req.session.success = false;
      res.redirect('/sender/delete'+id);
    }else{
      req.getConnection((err,conn) =>{
        conn.query('DELETE FROM sender WHERE sender_id=?',[id],(err,sender_confirmdelete)=>{
          if (err){

            req.session.test = "ไม่สามารถลบได้";
            req.session.success = false;

            res.redirect('/sender');
            return;
          }else {
            req.session.success = true;
            req.session.topic = "ลบข้อมูลสำเร็จ";

          }
          console.log(sender_confirmdelete);
          res.redirect('/sender/');
        });
      });
    }
  }
};

controller.edit = (req,res) => {
  const data = req.body;
  const {id} = req.params;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM sender where sender_id = ?',[id],(err,sender_edit)=>{
        res.render('sender_edit',{
          data:sender_edit  , session: req.session
        });
      });
    });
  }
};

controller.save = (req,res) => {
  const {id} = req.params ;
  const data = req.body;
  const errors = validationResult(req) ;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    if (!errors.isEmpty()) {
      req.session.errors = errors;
      req.session.success = false;
      res.redirect('/sender/edit/'+id)
    } else {
      req.session.success = true;
      req.session.topic = "แก้ไขข้อมูลสำเร็จ";
      req.getConnection((err,conn) => {
        conn.query('UPDATE sender SET ? WHERE sender_id = ? ',[data,id],(err,sender_save)=>{
          if (err){
            res.json(err);
          }
          console.log(data);
          res.redirect('/sender/');
        });
      });
    }
  }
}

module.exports = controller;
