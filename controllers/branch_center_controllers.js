const controller = {};
const {validationResult} = require('express-validator');

controller.list = (req,res) => {
  if(typeof req.session.userid == 'undefined'){ res.redirect('/');}else {
    req.getConnection((err,conn) =>{
      conn.query('SELECT o.oil_id,o.d1,o.d2,o.uig,o.g95,o.g91,o.e20,o.e85,o.branch_id,o.dp,b.branch_name,b.branch_id,date_format(o.date,"%d/%m/%Y") as date FROM oil_list as o JOIN branch as b on o.branch_id = b.branch_id WHERE date = CURDATE()-1',(err,oil_list)=>{
        conn.query('SELECT o.oil_id,date_format(o.date,"%d/%m/%Y") as date1 FROM oil_list as o GROUP BY o.date DESC',(err,oil_list2)=>{
          conn.query('SELECT c.cdate,c.center_id,b.branch_name,c.cd1,c.cd2,c.cdp,c.cuig,c.cg95,c.cg91,c.ce20,c.ce85,s.sender_name,od.order_name,date_format(c.cdate,"%d/%m/%Y") as cdate FROM oil_list as o JOIN branch as b on o.branch_id = b.branch_id JOIN branch_center as c on c.oil_id = o.oil_id JOIN branch_order as od on c.order_id = od.order_id JOIN sender as s on c.sender_id = s.sender_id WHERE cdate= CURDATE() ORDER BY c.center_id ASC',(err,oil_list3)=>{
            if(err){
              res.json(err);
            }
            res.render('branch_center_list',{
              data:oil_list ,data1:oil_list2,data3:oil_list3,session:req.session
            });
          });
        });
      });
    });
  }
};
controller.check = (req,res) => {
  const {date}=req.body;
  const {id} = req.params;
  if(typeof req.session.userid == 'undefined'){ res.redirect('/');}else {
    req.getConnection((err,conn) =>{
      conn.query('SELECT o.oil_id,o.d1,o.d2,o.uig,o.g95,o.g91,o.e20,o.e85,o.branch_id,o.dp,b.branch_name,b.branch_id,date_format(o.date,"%d/%m/%Y") as date FROM oil_list as o JOIN branch as b on o.branch_id = b.branch_id WHERE date_format(o.date,"%d/%m/%Y") = ?',[date],(err,data)=>{
        conn.query('SELECT o.oil_id,date_format(o.date,"%d/%m/%Y") as date1 FROM oil_list as o GROUP BY o.date DESC',(err,data1)=>{
          conn.query('SELECT c.cdate,c.center_id,b.branch_name,c.cd1,c.cd2,c.cdp,c.cuig,c.cg95,c.cg91,c.ce20,c.ce85,s.sender_name,od.order_name,date_format(c.cdate,"%d/%m/%Y") as cdate FROM oil_list as o JOIN branch as b on o.branch_id = b.branch_id JOIN branch_center as c on c.oil_id = o.oil_id JOIN branch_order as od on c.order_id = od.order_id JOIN sender as s on c.sender_id = s.sender_id WHERE date_format(ADDDATE(c.cdate,-1),"%d/%m/%Y") = ? ORDER BY c.center_id ASC',[date],(err,data3)=>{
            if(err){
              res.json(err);
            }
            res.render('branch_center_check',{
              data:data,data1:data1,data3:data3,session:req.session
            });
          });
        });
      });
    });
  }
};

controller.new = (req,res) => {
  if(typeof req.session.userid2 == 'null'){
    res.redirect('/');
  }else {
    if(typeof req.session.userid == 'null'){
      res.redirect('/');
    }else {
      const data = null ;
      if(typeof req.session.userid == 'undefined'){
        res.redirect('/');
      }else {
        req.getConnection((err,conn) => {
          conn.query('SELECT * FROM branch_center ',(err,data)=>{
            conn.query('SELECT o.oil_id,b.branch_id,b.branch_name FROM oil_list as o RIGHT JOIN branch as b on o.branch_id = b.branch_id GROUP BY branch_id',(err,data3)=>{

              conn.query('SELECT * FROM sender ',(err,data5)=>{
                conn.query('SELECT * FROM branch_order ',(err,data6)=>{
                  conn.query('SELECT * FROM branch ',(err,data7)=>{
                    //res.json(data3);
                    res.render('branch_center_new',{
                      data:data, data3:data3 , data5:data5, data6:data6,data7:data7,session: req.session
                    });
                  });
                });
              });
            });
          });
        });
      };
    };
  }
};
controller.add = (req,res) => {
  const data = req.body;
  if(data.oil_id=="กรุณาเลือก"){
    data.oil_id=null;
  }
  if(data.sender_id=="กรุณาเลือก"){
    data.sender_id=null;
  }
  if(data.order_id=="กรุณาเลือก"){
    data.order_id=null;
  }
  //  res.json(data);
  const errors = validationResult(req);
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    if(!errors.isEmpty()){
      req.session.errors = errors;
      req.session.success = false;
      res.redirect('/branch_center/new');
    }else{
      req.session.success = true;
      req.session.topic = "เพิ่มข้อมูลสำเร็จ";

      req.getConnection((err,conn) =>{
        conn.query('SELECT o.oil_id as id FROM oil_list as o JOIN branch as b on o.branch_id = b.branch_id JOIN branch_center as c on c.oil_id = o.oil_id JOIN branch_order as od on c.order_id = od.order_id JOIN sender as s on c.sender_id = s.sender_id WHERE cdate= ? && o.oil_id=?',[data.cdate,data.oil_id],(err,data2) =>{
          if(data2.length>0){
            if (data.oil_id == data2[0].id) {
              req.session.test = "ไม่สามารถเพิ่มข้อมูลได้ เนื่องจากมีข้อมูลแล้ว กรุณาลบ หรือ แก้ไข";
              req.session.success = false;
              res.redirect('/branch_center/new');
            }
          }else if(data2){
            req.getConnection((err,conn) =>{
              conn.query('INSERT INTO branch_center set ? ',[data],(err,oil_new) =>{
                if (err){
                  res.json(err);
                }
                res.redirect('/branch_center');
              });
            });
          }
        });
      });
    }
  }
};

controller.edit = (req,res) => {
  const data = req.body;
  const {id} = req.params;
  if(typeof req.session.userid == 'undefined'){ res.redirect('/');}else {
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM branch_center WHERE center_id = ?',[id],(err,data)=>{

        conn.query('SELECT o.oil_id,b.branch_id,b.branch_name FROM oil_list as o RIGHT JOIN branch as b on o.branch_id = b.branch_id GROUP BY branch_id',(err,data2)=>{
          conn.query('SELECT * FROM sender ',(err,data4)=>{
            conn.query('SELECT * FROM branch_order ',(err,data5)=>{
              conn.query('SELECT o.date FROM oil_list as o GROUP BY o.date DESC',(err,data6)=>{
                conn.query('SELECT date_format(c.cdate,"%Y-%m-%d") as cdate FROM oil_list as o JOIN branch_center as c on o.oil_id = c.oil_id WHERE center_id = ?',[id],(err,data7)=>{

                  res.render('branch_center_edit',{
                    data:data , data2:data2 , data4:data4, data5:data5, data6:data6, data7:data7,session: req.session
                  });
                });
              });
            });
          });
        });
      });
    });
  }
};

controller.save = (req,res) => {
  const {id} = req.params ;
  const data = req.body;
  //res.json(data);
  const errors = validationResult(req) ;
  if(typeof req.session.userid == 'undefined'){ res.redirect('/');}else {
    if(!errors.isEmpty()){
      req.session.errors = errors;
      req.session.success = false;
      res.redirect('/branch_center/edit/'+id);
    }else{
      req.session.success = true;
      req.session.topic = "แก้ไขข้อมูลสำเร็จ";
      req.getConnection((err,conn) => {
        conn.query('UPDATE branch_center SET ? WHERE center_id = ? ',[data,id],(err,checklist_save)=>{
          if (err){
            res.json(err);
          }
          console.log(data);
          res.redirect('/branch_center');
        });
      });
    }
  }
}

controller.delete = (req,res) => {
  const {id} = req.params ;
  if(typeof req.session.userid == 'undefined'){ res.redirect('/');}else {
    req.getConnection((err,conn) => {
      conn.query('SELECT c.center_id,b.branch_name,c.cd1,c.cd2,c.cdp,c.cuig,c.cg95,c.cg91,c.ce20,c.ce85,s.sender_name,od.order_name,c.description FROM oil_list as o JOIN branch as b on o.branch_id = b.branch_id JOIN branch_center as c on c.oil_id = o.oil_id JOIN branch_order as od on c.order_id = od.order_id JOIN sender as s on c.sender_id = s.sender_id where center_id = ?',[id],(err,data)=>{
        conn.query('SELECT *  FROM oil_list as o GROUP BY o.date DESC',(err,oil_list2)=>{

          if (err){
            res.json(err);
          }
          res.render('branch_center_delete',{
            data:data , data2:id,session: req.session
          });
        });
      });
    });
  }
};
controller.confirmdelete = (req,res) => {
  const {id}=req.params;
  const errors = validationResult(req);
  if(typeof req.session.userid == 'undefined'){ res.redirect('/');}else {
    if(!errors.isEmpty()){
      req.session.errors = errors;
      req.session.success = false;
      res.redirect('/branch_center/delete'+id);
    }else{
      req.getConnection((err,conn) =>{
        conn.query('DELETE FROM branch_center WHERE center_id=?',[id],(err,checklist)=>{
          if (err){
            req.session.success = true;
            req.session.topic = "ลบข้อมูลสำเร็จ";

          }
          console.log(checklist);
          res.redirect('/branch_center');
        });
      });
    }
  }
};


//add oil user

controller.newo = (req,res) => {
  const data = null ;
  if(typeof req.session.userid == 'undefined'){ res.redirect('/');}else {
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM oil_list',(err,oil_new1)=>{
        conn.query('SELECT * FROM branch',(err,oil_new3)=>{
          conn.query('SELECT o.date FROM oil_list as o GROUP BY o.date DESC',(err,oil_new2)=>{
            res.render('oil_b_new',{

              data1:oil_new1,data2:oil_new2 ,data3:oil_new3 , session: req.session
            });
          });
        });
      });
    });
  }
};

controller.addo = (req,res) => {
  const data = req.body;
  //res.json(data);
  const errors = validationResult(req);
  if(typeof req.session.userid == 'undefined'){ res.redirect('/');}else {
    if(!errors.isEmpty()){
      req.session.errors = errors;
      req.session.success = false;
      res.redirect('/oil_b/new');
    }else{
      req.session.success = true;
      req.session.topic = "เพิ่มข้อมูลสำเร็จ";

      req.getConnection((err,conn) =>{
        conn.query('SELECT*FROM oil_list as o JOIN branch as b ON b.branch_id=o.branch_id WHERE o.date = ? && b.branch_id =?',[data.date,data.branch_id],(err,data2) =>{

          if(data2.length>0){
            if (data.oil_id == data2[0].id) {
              req.session.test = "ไม่สามารถเพิ่มข้อมูลได้";
              req.session.success = false;
              res.redirect('/oil_b/new');
            }
          }else if(data2){
            req.getConnection((err,conn) =>{
              conn.query('INSERT INTO oil_list set ? ',[data],(err,oil_new) =>{
                if (err){
                  res.json(err);
                }
                res.redirect('/branch_center');
              });
            });
          }
        });
      });
    }
  }
};

controller.edito = (req,res) => {
  const data = req.body;
  const {id} = req.params;
  // database add ใส่ดาต้าเบสตรงนี้
  if(typeof req.session.userid == 'undefined'){ res.redirect('/');}else {
    req.getConnection((err,conn) => {
      conn.query('SELECT o.oil_id,o.d1,o.d2,o.uig,o.g95,o.g91,o.e20,o.e85,o.branch_id,o.dp,b.branch_name,b.branch_id,date_format(o.date,"%Y-%m-%d") as date FROM oil_list as o JOIN branch as b on o.branch_id = b.branch_id WHERE o.oil_id = ?',[id],(err,data)=>{
        conn.query('SELECT * FROM branch ',(err,data2)=>{
          conn.query('SELECT o.date FROM oil_list as o GROUP BY o.date DESC',(err,data3)=>{


            res.render('oil_b_edit',{
              data:data , data2:data2 , data3:data3,session: req.session
            });
          });
        });
      });
    });
  }
};

controller.saveo = (req,res) => {
  const {id} = req.params ;
  const data = req.body;
  const errors = validationResult(req) ;
  if(typeof req.session.userid == 'undefined'){ res.redirect('/');}else {
    if(!errors.isEmpty()){
      req.session.errors = errors;
      req.session.success = false;
      res.redirect('/oil_b/edit/'+id);
    }else{
      req.session.success = true;
      req.session.topic = "แก้ไขข้อมูลสำเร็จ";
      req.getConnection((err,conn) => {
        conn.query('UPDATE oil_list SET ? WHERE oil_id = ? ',[data,id],(err,checklist_save)=>{
          if (err){
            res.json(err);
          }
          console.log(data);
          res.redirect('/branch_center');
        });
      });
    }
  }
};

controller.deleteo = (req,res) => {
  const {id} = req.params ;
  if(typeof req.session.userid == 'undefined'){ res.redirect('/');}else {
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM oil_list as o JOIN branch as b on o.branch_id = b.branch_id where oil_id = ?',[id],(err,data)=>{
        conn.query('SELECT *  FROM oil_list as o GROUP BY o.date DESC',(err,oil_list2)=>{

          if (err){
            res.json(err);
          }
          res.render('oil_b_delete',{
            data:data , data2:id,session: req.session
          });
        });
      });
    });
  }
};

controller.confirmdeleteo = (req,res) => {
  const {id}=req.params;
  const errors = validationResult(req);
  if(typeof req.session.userid == 'undefined'){ res.redirect('/');}else {

    req.getConnection((err,conn) =>{
      conn.query('DELETE FROM oil_list WHERE oil_id=?',[id],(err,checklist)=>{
        console.log(checklist);
        res.redirect('/branch_center');
      });
    });

  }
};
module.exports = controller;
