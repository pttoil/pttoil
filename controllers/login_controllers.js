const controller = {};
const { validationResult } = require('express-validator');

controller.list = (req,res) => {
  const data = req.body;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM login ',(err,login_list)=>{
        if(err){
          res.json(err);
        }

        res.render('login_list',{
          data:login_list,session: req.session
        });
      });
    });
  }
};

controller.new = (req,res) => {
  const data = null ;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM login ',(err,login_new)=>{
        res.render('login_new',{
          data2:login_new , session: req.session
        });
      });
    });
  }
};

controller.add = (req,res) => {
  const data = req.body;
  const errors = validationResult(req) ;
  if(data.enable=="on"){
    data.enable=1;
  } else {
    data.enable=0;
  }
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    if(!errors.isEmpty()){
      req.session.errors = errors;
      req.session.success = false;
      res.redirect('/login/new');
    }else{
      req.session.success = true;
      req.session.topic = "เพิ่มข้อมูลสำเร็จ";
      req.getConnection((err,conn) =>{
        conn.query('INSERT INTO login set ?',[data],(err,login_add) =>{
          console.log(login_add);
          res.redirect('/login');
        });
      });
    }
  }
};

controller.delete = (req,res) => {
  const {id} = req.params ;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM login where login_id = ?',[id],(err,login_delete)=>{
        if (err){
          res.json(err);
        }
        res.render('login_delete',{
          data1:login_delete ,session: req.session
        });
      });
    });
  }
};

controller.confirmdelete = (req,res) => {
  const {id}=req.params;
  const errors = validationResult(req);
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    if(!errors.isEmpty()){
      req.session.errors = errors;
      req.session.success = false;
      res.redirect('/login/delete'+id);
    }else{
      req.getConnection((err,conn) =>{
        conn.query('DELETE FROM login WHERE login_id=?',[id],(err,login_confirmdelete)=>{
          if (err){

            req.session.test = "ไม่สามารถลบได้";
            req.session.success = false;

            res.redirect('/login');
            return;
          }else {
            req.session.success = true;
            req.session.topic = "ลบข้อมูลสำเร็จ";

          }
          console.log(login_confirmdelete);
          res.redirect('/login/');
        });
      });
    }
  }
};

controller.edit = (req,res) => {
  const data = req.body;
  const {id} = req.params;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM login ' ,[id],(err,login)=>{
        conn.query('SELECT * FROM login where login_id = ?',[id],(err,login_edit)=>{
          res.render('login_edit',{
            data:login_edit ,data1:login , session: req.session
          });
        });
      });
    });
  }
};

controller.save = (req,res) => {
  const {id} = req.params ;
  const data = req.body;
  const errors = validationResult(req) ;
  if(data.enable=="on"){
    data.enable=1;
  } else {
    data.enable=0;
  }
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    if (!errors.isEmpty()) {
      req.session.errors = errors;
      req.session.success = false;
      res.redirect('/login/edit/'+id)
    } else {
      req.session.success = true;
      req.session.topic = "แก้ไขข้อมูลสำเร็จ";
      req.getConnection((err,conn) => {
        conn.query('UPDATE login SET ? WHERE login_id = ? ',[data,id],(err,login_save)=>{
          if (err){
            res.json(err);
          }
          console.log(data);
          res.redirect('/login/');
        });
      });
    }
  }
}

module.exports = controller;
