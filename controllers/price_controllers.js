const controller = {};
// const { validationResult } = require('express-validator');

controller.list = (req,res) => {
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    const data = req.body;
    req.getConnection((err,conn) =>{
      conn.query('SELECT p.price_id,date_format(p.pdate,"%d/%m/%Y") as pdate,p.diesel,p.dp,p.e85,p.e20,p.g91,p.g95,p.ulg FROM price as p WHERE date_format(p.pdate,"%Y-%m-%d")<= CURDATE() ORDER by p.pdate desc LIMIT 1',(err,price_list)=>{
        conn.query('SELECT p.price_id,date_format(p.pdate,"%d/%m/%Y") as date1 FROM price as p GROUP BY p.pdate DESC',(err,price_list2)=>{
          conn.query('SELECT o.oil_id,date_format(o.date,"%d/%m/%Y") as date2,date_format(o.date,"%Y-%m-%d") as date1 FROM oil_list as o GROUP BY o.date DESC',(err,oil_list4)=>{
            conn.query('SELECT p.price_id,date_format(p.pdate,"%Y-%m-%d") as date1,date_format(p.pdate,"%d/%m/%Y") as date2 FROM price as p GROUP BY p.pdate DESC',(err,price_list3)=>{
              if(err){
                res.json(err);
              }
              res.render('price_list',{
                data:price_list,data1:price_list2,data2:price_list3,data3:oil_list4,session: req.session
              });
            });
          });
        });
      });
    });
  }
};

controller.check = (req,res) => {
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    const data =req.body;
    const {id} = req.params;

    req.getConnection((err,conn) =>{
      conn.query('SELECT p.price_id,date_format(p.pdate,"%d/%m/%Y") as pdate,p.diesel,p.dp,p.e85,p.e20,p.g91,p.g95,p.ulg FROM price as p WHERE date_format(p.pdate,"%Y-%m-%d")<=? ORDER by p.pdate desc LIMIT 1 ',[data.date],(err,check)=>{
        //res.json(data);          // res.json(check);
        //conn.query('SELECT p.price_id,date_format(p.pdate,"%d/%m/%Y") as date1 FROM price as p GROUP BY p.pdate DESC',(err,check2)=>{
        //conn.query('SELECT p.price_id,date_format(p.pdate,"%Y-%m-%d") as date1,date_format(p.pdate,"%d/%m/%Y") as date2 FROM price as p GROUP BY p.pdate DESC',(err,price_list3)=>{
        if(err){
          res.json(err);
        }
        res.render('price_check',{
          data:check,data2:data.date,session:req.session
          //data:check,data1:check2,data2:price_list3,session:req.session
        });
        //});
        //});
      });
    });
  }
};

controller.new = (req,res) => {
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    const data = null ;
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM price ',(err,price_new)=>{
        res.render('price_new',{
          data2:price_new , session: req.session
        });
      });
    });
  }
};

controller.add = (req,res) => {
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    const data = req.body;
    req.getConnection((err,conn) =>{
      conn.query('INSERT INTO price set ?',[data],(err,price_add) =>{
        console.log(price_add);
        res.redirect('/price');
      });
    });
  }
};

controller.delete = (req,res) => {
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    const {id} = req.params ;
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM price where price_id = ?',[id],(err,price_delete)=>{
        conn.query('SELECT date_format(p.pdate,"%Y-%m-%d") as pdate FROM price as p WHERE price_id = ?',[id],(err,price_delete2)=>{
          if (err){
            res.json(err);
          }
          res.render('price_delete',{
            data:price_delete,data2:price_delete2 ,session: req.session
          });
        });
      });
    });
  }
};

controller.confirmdelete = (req,res) => {
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    const {id}=req.params;
    req.getConnection((err,conn) =>{
      conn.query('DELETE FROM price WHERE price_id=?',[id],(err,price_confirmdelete)=>{
        console.log(price_confirmdelete);
        res.redirect('/price/');
      });
    });
  }
};

controller.edit = (req,res) => {
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    const data = req.body;
    const {id} = req.params;
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM price where price_id = ?',[id],(err,price_edit)=>{
        conn.query('SELECT date_format(p.pdate,"%Y-%m-%d") as pdate FROM price as p WHERE price_id = ?',[id],(err,price_edit2)=>{
          res.render('price_edit',{
            data:price_edit,data2:price_edit2, session: req.session
          });
        });
      });
    });
  }
};

controller.save = (req,res) => {
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    const {id} = req.params ;
    const data = req.body;
    req.getConnection((err,conn) => {
      conn.query('UPDATE price SET ? WHERE price_id = ? ',[data,id],(err,price_save)=>{
        if (err){
          res.json(err);
        }
        console.log(price_save);
        res.redirect('/price/');
      });
    });
  }
};

controller.differentlist = (req,res) => {
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    const data =req.body;
    const {id} = req.params;
    req.getConnection((err,conn) =>{
      conn.query('SELECT date_format(DATE_ADD(( CURDATE()),INTERVAL -1 DAY),"%Y-%m-%d") AS DT',(err,date)=>{
        conn.query('SELECT date_format(DATE_ADD(( CURDATE()),INTERVAL -2 DAY),"%Y-%m-%d") AS DT',(err,date1)=>{
          conn.query('SELECT p.price_id,date_format(p.pdate,"%Y-%m-%d") as pdate,p.diesel,p.dp,p.e85,p.e20,p.g91,p.g95,p.ulg FROM price as p WHERE pdate IN (SELECT MAX(pdate) FROM price as p WHERE p.pdate <= ?)ORDER by pdate ASC',[date[0].DT],(err,data)=>{
            conn.query('SELECT p.price_id,date_format(p.pdate,"%d/%m/%Y") as pdate,p.diesel,p.dp,p.e85,p.e20,p.g91,p.g95,p.ulg FROM price as p WHERE pdate IN (SELECT MAX(pdate) FROM price as p WHERE p.pdate <= ?)ORDER by pdate ASC',[date1[0].DT],(err,data2)=>{
              conn.query('SELECT date_format(DATE_ADD(( CURDATE()),INTERVAL-1 DAY),"%d/%m/%Y") AS DT',(err,date3)=>{
                res.render('price_differencelist',{data:data,data2:data2,session:req.session,date3:date3
                });
              });
            });
          });
        });
      });
    });
  }
};
controller.different = (req,res) => {
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    const data =req.body;
    const {id} = req.params;
    req.getConnection((err,conn) =>{
      conn.query('SELECT p.price_id,date_format(p.pdate,"%d/%m/%Y") as pdate,p.diesel,p.dp,p.e85,p.e20,p.g91,p.g95,p.ulg FROM price as p WHERE p.pdate=?',[data.pdate],(err,check)=>{
        conn.query('SELECT date_format(p.pdate,"%d/%m/%Y") as date1,sum(if(p.pdate=?,p.diesel*-1,p.diesel)) as diesel,sum(if(p.pdate=?,p.e85*-1,p.e85)) as e85,sum(if(p.pdate=?,p.e20*-1,p.e20)) as e20,sum(if(p.pdate=?,p.g91*-1,p.g91)) as g91,sum(if(p.pdate=?,p.g95*-1,p.g95)) as g95,sum(if(p.pdate=?,p.ulg*-1,p.ulg)) as ulg,sum(if(p.pdate=?,p.dp*-1,p.dp)) as dp FROM price as p WHERE p.pdate BETWEEN date_add(?, INTERVAL -1 DAY) AND ?',[data.pdate,data.pdate,data.pdate,data.pdate,data.pdate,data.pdate,data.pdate,data.pdate,data.pdate],(err,pricelist)=>{
          conn.query('SELECT date_format(p.pdate,"%d/%m/%Y") as date1,date_format(p.pdate,"%Y-%m-%d") as pdate,p.price_id,p.diesel,p.e85,p.e20,p.g91,p.g95,p.ulg,p.dp FROM price as p WHERE p.pdate <= ? ORDER BY p.pdate desc LIMIT 2',[data.pdate],(err,priceall)=>{
            conn.query('SELECT date_format(p.pdate,"%d/%m/%Y") as date1,(p.diesel-?) as diesel,(p.e85-?) as e85,(p.e20-?) as e20,(p.g91-?) as g91,(p.g95-?) as g95,(p.ulg-?) as ulg,(p.dp-?) as dp FROM price as p WHERE p.pdate <=?  ORDER BY p.pdate DESC LIMIT 1',[priceall[1].diesel,priceall[1].e85,priceall[1].e20,priceall[1].g91,priceall[1].g95,priceall[1].ulg,priceall[1].dp,data.pdate],(err,pricesumedit)=>{

              if(err){
                res.json(err);
              }
              res.render('price_difference',{
                data:check,pricelist:pricelist,pricesumedit:pricesumedit,priceall:priceall,datadate:data.pdate,session:req.session

              });
            });
          });
        });
      });
    });
  }
};

controller.profit = (req,res) => {
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    const data = req.body;
    const {id} = req.params;
    req.getConnection((err,conn) => {
      conn.query('SELECT date_format(o.date,"%d/%m/%Y") as odate,SUM(o.d1+o.d2)AS diesel,SUM(o.e85) as e85,SUM(o.g91) as g91,SUM(o.g95) as g95,SUM(o.uig) as uig,SUM(o.dp) as dp,SUM(o.e20) as e20,p.diesel as pdiesel,p.e85 as pe85,p.g91 as pg91,p.g95 as pg95,p.ulg as pulg,p.dp as pdp,p.e20 as pe20 FROM oil_list as o LEFT JOIN price as p ON p.pdate=o.date GROUP by o.date ORDER BY o.date ASC',(err,data2)=>{
        res.render('profit_list',{
          data2:data2, session: req.session
        });
      });
    });
  }
};

controller.checkprofit = (req,res) => {
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    const data = req.body;
    req.getConnection((err,conn) => {
      conn.query('SELECT date_format(DATE_ADD((?),INTERVAL -1 DAY),"%Y-%m-%d") AS DT',[data.day1],(err,data5)=>{
        conn.query('SELECT MAX(date) as DT FROM oil_list WHERE date <= ?',[data5[0].DT],(err,data4)=>{
          //res.json(data4);
          conn.query('SELECT date_format(o.date,"%d/%m/%Y") as odate,SUM(o.d1+o.d2)AS diesel,SUM(o.e85) as e85,SUM(o.g91) as g91,SUM(o.g95) as g95,SUM(o.uig) as uig,SUM(o.dp) as dp,SUM(o.e20) as e20,p.diesel as pdiesel,p.e85 as pe85,p.g91 as pg91,p.g95 as pg95,p.ulg as pulg,p.dp as pdp,p.e20 as pe20 FROM oil_list as o LEFT JOIN price as p ON p.pdate=o.date WHERE o.date BETWEEN ? AND ? GROUP by o.date ORDER BY o.date ASC',[data.day1,data.day2],(err,data2)=>{
            //res.json(data2);
            conn.query('SELECT date_format(DATE_ADD((?),INTERVAL 0 DAY),"%d/%m/%Y") AS DT',[data.day1],(err,data3)=>{
              conn.query('SELECT date_format(DATE_ADD((?),INTERVAL 0 DAY),"%d/%m/%Y") AS DT',[data.day2],(err,data1)=>{

                res.render('profit_check',{
                  data:data,data1:data1,data2:data2,data3:data3,session:req.session
                });
              });
            });
          });
        });
      });
    });
  }
};

controller.totle = (req,res) => {
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    const data = req.body;
    //res.json(data);
    req.getConnection((err,conn) => {
      conn.query('SELECT date_format(o.date,"%d/%m/%Y")as date,o.d1,o.d2,o.uig,o.g95,o.g91,o.e20,o.e85,o.dp,b.branch_name FROM oil_list as o JOIN branch as b ON b.branch_id=o.branch_id WHERE o.date in (SELECT MAX(date) FROM oil_list as o WHERE o.date<= ?) ',[data.pdate],(err,data1)=>{
        conn.query('SELECT date_format(o.date,"%Y-%m-%d")as date,o.d1,o.d2,o.uig,o.g95,o.g91,o.e20,o.e85,o.dp,b.branch_name FROM oil_list as o JOIN branch as b ON b.branch_id=o.branch_id WHERE o.date in (SELECT MAX(date) FROM oil_list as o WHERE o.date<= ?) ',[data.pdate],(err,data3)=>{
          conn.query('SELECT date_format(DATE_ADD((?),INTERVAL 1 DAY),"%Y-%m-%d") AS DT',[data.pdate],(err,test)=>{
            conn.query('SELECT * FROM price WHERE pdate IN (SELECT  MAX(pdate) FROM price as p WHERE p.pdate <= ?)ORDER by pdate ASC',[test[0].DT],(err,data2)=>{


              res.render('price_totle',{
                data1:data1,data2:data2,session: req.session
              });
            });
          });
        });
      });
    });
  }
};

controller.totlelist = (req,res) => {
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    const data = req.body;
    //res.json(data);
    req.getConnection((err,conn) => {
      conn.query('SELECT date_format(o.date,"%d/%m/%Y")as date,o.d1,o.d2,o.uig,o.g95,o.g91,o.e20,o.e85,o.dp,b.branch_name FROM oil_list as o JOIN branch as b ON b.branch_id=o.branch_id WHERE o.date in (SELECT MAX(date) FROM oil_list as o WHERE o.date<= CURDATE()) ',[data.pdate],(err,data1)=>{
        conn.query('SELECT date_format(o.date,"%Y-%m-%d")as date,o.d1,o.d2,o.uig,o.g95,o.g91,o.e20,o.e85,o.dp,b.branch_name FROM oil_list as o JOIN branch as b ON b.branch_id=o.branch_id WHERE o.date in (SELECT MAX(date) FROM oil_list as o WHERE o.date<= CURDATE()) ',[data.pdate],(err,data3)=>{
          conn.query('SELECT date_format(DATE_ADD((CURDATE()),INTERVAL 1 DAY),"%Y-%m-%d") AS DT',[data.pdate],(err,test)=>{
            conn.query('SELECT * FROM price WHERE pdate IN (SELECT  MAX(pdate) FROM price as p WHERE p.pdate <= ?)ORDER by pdate ASC',[test[0].DT],(err,data2)=>{
              res.render('price_totle',{
                data1:data1,data2:data2,session: req.session
              });
            });
          });
        });
      });
    });
  }
};

controller.sum = (req,res) => {
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) => {
      conn.query('SELECT date_format(DATE_ADD((CURDATE()),INTERVAL -1 DAY),"%Y-%m-%d") AS DT',(err,date)=>{
        conn.query('SELECT date_format(o.date,"%d/%m/%Y")as date,o.d1,o.d2,o.uig,o.g95,o.g91,o.e20,o.e85,o.dp,b.branch_name FROM oil_list as o JOIN branch as b ON b.branch_id=o.branch_id WHERE o.date in (SELECT MAX(date) FROM oil_list as o WHERE o.date<=?) ',[date[0].DT],(err,data1)=>{
          conn.query('SELECT date_format(o.date,"%Y-%m-%d")as date,o.d1,o.d2,o.uig,o.g95,o.g91,o.e20,o.e85,o.dp,b.branch_name FROM oil_list as o JOIN branch as b ON b.branch_id=o.branch_id WHERE o.date in (SELECT MAX(date) FROM oil_list as o WHERE o.date<=?) ',[date[0].DT],(err,data3)=>{
            conn.query('SELECT date_format(DATE_ADD((?),INTERVAL +1 DAY),"%Y-%m-%d") AS DT',[date[0].DT],(err,test)=>{
              conn.query('SELECT * FROM price WHERE pdate IN (SELECT  MAX(pdate) FROM price as p WHERE p.pdate <= ?)ORDER by pdate ASC',[test[0].DT],(err,data2)=>{
                conn.query('SELECT * FROM debt as d WHERE d.date in (SELECT MAX(date) FROM debt as d WHERE d.date <= ?)',[test[0].DT],(err,debt)=>{
                  conn.query('SELECT SUM(bl.balance) as balance FROM balance as bl JOIN bank as b ON b.id = bl.bank_id WHERE bl.date = ?',[test[0].DT],(err,balance)=>{
                    conn.query('SELECT bc.cdate,(SUM(cd1)+SUM(cd2)) as cd,SUM(cdp)as cdp,SUM(cuig)as cuig,SUM(cg95)as cg95,SUM(cg91)as cg91,SUM(ce20)as ce20,SUM(ce85) as ce85 FROM branch_center as bc WHERE bc.cdate = ?',[test[0].DT],(err,bc)=>{
                      conn.query('SELECT * FROM price WHERE pdate IN (SELECT  MAX(pdate) FROM price as p WHERE p.pdate <=?)ORDER by pdate ASC',[test[0].DT],(err,price)=>{
                        conn.query('SELECT date_format(DATE_ADD((?),INTERVAL +1 DAY),"%d-%m-%Y") AS DT',[date[0].DT],(err,date)=>{
                          res.render('sum',{
                            data1:data1,data2:data2,session:req.session,data3:debt,data4:balance,data5:bc,data6:price,data7:date
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  }
};

controller.checksum = (req,res) => {
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    const data = req.body;
    var d = new Date();
    if (data.date == '') {
      data.date = d.toISOString().split('T')[0];
    }
    console.log(data);
    req.getConnection((err,conn) => {
      conn.query('SELECT date_format(DATE_ADD((?),INTERVAL 0 DAY),"%Y-%m-%d") AS DT',[data.date],(err,date)=>{
        conn.query('SELECT date_format(DATE_ADD((?),INTERVAL -1 DAY),"%Y-%m-%d") AS DT',[date[0].DT],(err,date1)=>{
          conn.query('SELECT date_format(o.date,"%d/%m/%Y")as date,o.d1,o.d2,o.uig,o.g95,o.g91,o.e20,o.e85,o.dp,b.branch_name FROM oil_list as o JOIN branch as b ON b.branch_id=o.branch_id WHERE o.date in (SELECT MAX(date) FROM oil_list as o WHERE o.date<=?) ',[date1[0].DT],(err,data1)=>{
            conn.query('SELECT date_format(o.date,"%Y-%m-%d")as date,o.d1,o.d2,o.uig,o.g95,o.g91,o.e20,o.e85,o.dp,b.branch_name FROM oil_list as o JOIN branch as b ON b.branch_id=o.branch_id WHERE o.date in (SELECT MAX(date) FROM oil_list as o WHERE o.date<=?) ',[date1[0].DT],(err,data3)=>{
              conn.query('SELECT date_format(DATE_ADD((?),INTERVAL +1 DAY),"%Y-%m-%d") AS DT',[data3[0].date],(err,test)=>{
                conn.query('SELECT * FROM price WHERE pdate IN (SELECT  MAX(pdate) FROM price as p WHERE p.pdate <= ?)ORDER by pdate ASC',[test[0].DT],(err,data2)=>{
                  conn.query('SELECT * FROM debt as d WHERE d.date in (SELECT MAX(date) FROM debt as d WHERE d.date <= ?)',[data.date],(err,debt)=>{
                    conn.query('SELECT SUM(bl.balance) as balance,bl.date  FROM balance as bl JOIN bank as b ON b.id=bl.bank_id WHERE bl.date = ?',[date[0].DT],(err,balance)=>{
                      conn.query('SELECT bc.cdate,(SUM(cd1)+SUM(cd2)) as cd,SUM(cdp)as cdp,SUM(cuig)as cuig,SUM(cg95)as cg95,SUM(cg91)as cg91,SUM(ce20)as ce20,SUM(ce85) as ce85 FROM branch_center as bc WHERE bc.cdate = ?',[date[0].DT],(err,bc)=>{
                        conn.query('SELECT * FROM price WHERE pdate IN (SELECT  MAX(pdate) FROM price as p WHERE p.pdate <=?)ORDER by pdate ASC',[date[0].DT],(err,price)=>{
                          conn.query('SELECT date_format(DATE_ADD((?),INTERVAL 0 DAY),"%d-%m-%Y") AS DT',[date[0].DT],(err,date)=>{
                            res.render('checksum',{
                              data1:data1,data2:data2,session:req.session,data3:debt,data4:balance,data5:bc,data6:price,data7:date
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  }
};

controller.test = (req,res) => {
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    var data = req.body;
    var d = new Date();
    var dq = d.toISOString().split('T')[0];
    if (data.date) {
      d = new Date(data.date);
      dq = dq = d.toISOString().split('T')[0];
    }
    req.getConnection((err,conn) => {
      conn.query('SELECT oil_id as id, date_add(date,interval(1) day) as date, date_add(date,interval(2) day) as sdate, sum(d1) as d1,sum(d2) as d2,sum(uig) as uig,sum(g95) as g95,sum(g91) as g91,sum(e20) as e20,sum(e85) as e85,sum(dp) as dp, (sum(d1)+sum(d2)+sum(uig)+sum(g95)+sum(g91)+sum(e20)+sum(e85)+sum(dp)) as ground FROM pttoil.oil_list as o JOIN pttoil.branch as b ON b.branch_id = o.branch_id where o.date >= (select date_sub(?,interval 11 day) as ndate) group by date order by date desc',[dq],(err,ground)=>{
        conn.query('SELECT bc.center_id as id, bc.cdate,(SUM(cd1)+SUM(cd2)) as cd,SUM(cdp)as cdp,SUM(cuig)as cuig,SUM(cg95)as cg95,SUM(cg91)as cg91,SUM(ce20)as ce20,SUM(ce85) as ce85, (SUM(cd1)+SUM(cd2)+SUM(cdp)+SUM(cuig)+SUM(cg95)+SUM(cg91)+SUM(ce20)+SUM(ce85)) as trans FROM pttoil.branch_center as bc where cdate >= (select date_sub(?,interval 11 day)) group by cdate order by cdate desc',[dq],(err,transport)=>{
          conn.query('SELECT date, SUM(bl.balance) as balance FROM pttoil.balance as bl JOIN pttoil.bank as b ON b.id = bl.bank_id where date > (select date_sub(?,interval 30 day)) group by date order by date desc limit 11',[dq],(err,balance)=>{
            conn.query('select *  from pttoil.debt where date <= ? order by date desc limit 11',[dq],(err,debt)=>{
              conn.query('SELECT * FROM pttoil.price where pdate <= ? order by pdate desc limit 11',[dq],(err,price)=>{
                var day = d.getDate();
                var month = d.getMonth()+1;
                var year = d.getFullYear();
                var l = [31,28,31,30,31,30,31,31,30,31,30,31];
                var back_date = [];
                var keep = 0;
                for (var i = 0; i < 11; i++) {
                  if (i == 0) {
                    day = day-0;
                  }else {
                    day = day-1;
                  }
                  if (day == 0) {
                    month = parseInt(month)-1;
                    day = l[parseInt(month)-1];
                    if (month <= 0) {
                      month = 12;
                      year = year - 1;
                      day = l[parseInt(month)-1];
                    }
                    if (month == 2) {
                      if (year%4 == 0 && year%100 != 0 || year%400 == 0) {
                        day = 29;
                      }else {
                        day = l[parseInt(month)-1];
                      }
                    }
                    if (day < 10) {
                      day = '0'+day;
                    }
                    if (month < 10) {
                      month = '0'+month;
                    }
                  }else {
                    if (day < 10) {
                      day = '0'+parseInt(day);
                    }
                    month = month;
                    if (month < 10) {
                      month = '0'+parseInt(month);
                    }
                    year = year;
                  }
                  keep = year+'-'+month+'-'+day;
                  back_date.push({date:keep})
                }
                console.log(back_date);
                var gat1 = [];
                for (var i = 0; i < ground.length; i++) {
                  var u = 0;
                  for (var j = 0; j < price.length; j++) {
                    if (ground[i].date >= price[j].pdate) {
                      if (u == 0) {
                        u = 1
                        var send = ((ground[i].d1+ground[i].d2)*price[j].diesel)+(ground[i].uig*price[j].ulg)+(ground[i].g95*price[j].g95)+(ground[i].g91*price[j].g91)+(ground[i].e20*price[j].e20)+(ground[i].e85*price[j].e85)+(ground[i].dp*price[j].dp);
                      }
                    }
                  }
                  gat1.push({date:ground[i].date.toISOString().split('T')[0],sdate:ground[i].sdate.toISOString().split('T')[0],oil:ground[i].ground,send:send});
                }
                ///--------------///
                var gat2 = [];
                for (var i = 0; i < transport.length; i++) {
                  var u = 0;
                  for (var j = 0; j < price.length; j++) {
                    if (transport[i].cdate >= price[j].pdate) {
                      if (u == 0) {
                        u = 1
                        var send = (transport[i].cd*price[j].diesel)+(transport[i].cuig*price[j].ulg)+(transport[i].cg95*price[j].g95)+(transport[i].cg91*price[j].g91)+(transport[i].ce20*price[j].e20)+(transport[i].ce85*price[j].e85)+(transport[i].cdp*price[j].dp);
                      }
                    }
                  }
                  gat2.push({date:transport[i].cdate.toISOString().split('T')[0],oil:transport[i].trans,send:send});
                }
                var last = [];
                var forsub = [];
                for (var i = 0; i < back_date.length; i++) {
                  var d1oil = 0; var d1send = 0; var sdate = 0;
                  for (var j = 0; j < gat1.length; j++) {
                    if (gat1[j].date == back_date[i].date) {
                      sdate = gat1[j].sdate;
                      d1oil = gat1[j].oil;
                      d1send = gat1[j].send;
                    }
                  }
                  var d2oil = 0; var d2send = 0;
                  for (var j = 0; j < gat2.length; j++) {
                    if (gat2[j].date == back_date[i].date) {
                      d2oil = gat2[j].oil;
                      d2send = gat2[j].send;
                    }
                  }
                  var y = 0; var gbalance = 0;
                  for (var j = 0; j < balance.length; j++) {
                    if (balance[j].date.toISOString().split('T')[0] == back_date[i].date) {
                      y = 1; gbalance = balance[j].balance;
                    }else if (y == 0 && j == i) {
                      y = 1; gbalance = balance[j].balance;
                    }
                  }
                  var t = 0; var gdebt = 0;
                  for (var j = 0; j < debt.length; j++) {
                    if (debt[j].date.toISOString().split('T')[0] == back_date[i].date) {
                      t = 1; gdebt = debt[j].debt;
                    }else if (t == 0 && j == i) {
                      t = 1; gdebt = debt[j].debt;
                    }
                  }
                  last.push({date:back_date[i].date,d1oil:d1oil,d1send:d1send,d2oil:d2oil,d2send:d2send,balance:gbalance,debt:gdebt});
                  forsub.push({date:sdate,d1oil:d1oil,d1send:d1send,d2oil:d2oil,d2send:d2send,balance:gbalance,debt:gdebt});
                }
                // console.log(gat1,gat2);
                console.log(forsub);
                // console.log(balance);
                res.render('test',{
                  data1:gat1,data2:gat2,session:req.session,data3:balance,data4:debt,data5:price,back_date:back_date,last:last,forsub:forsub
                });
              });
            });
          });
        });
      });
    });
  }
};

module.exports = controller;
