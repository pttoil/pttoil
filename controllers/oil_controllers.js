const controller = {};
const {validationResult} = require('express-validator');

controller.list = (req,res) => {
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) =>{
      conn.query('SELECT o.oil_id,o.d1,o.d2,o.uig,o.g95,o.g91,o.e20,o.e85,o.branch_id,o.dp,b.branch_name,b.branch_id,date_format(o.date,"%d/%m/%Y") as date FROM oil_list as o JOIN branch as b on o.branch_id = b.branch_id WHERE date = CURDATE()-1',(err,oil_list)=>{
        conn.query('SELECT o.oil_id,date_format(o.date,"%d/%m/%Y") as date1 FROM oil_list as o GROUP BY o.date DESC',(err,oil_list2)=>{
          conn.query('SELECT c.cdate,c.center_id,b.branch_name,c.cd1,c.cd2,c.cdp,c.cuig,c.cg95,c.cg91,c.ce20,c.ce85,s.sender_name,od.order_name,date_format(c.cdate,"%d/%m/%Y") as cdate FROM oil_list as o JOIN branch as b on o.branch_id = b.branch_id JOIN branch_center as c on c.oil_id = o.oil_id JOIN branch_order as od on c.order_id = od.order_id JOIN sender as s on c.sender_id = s.sender_id WHERE cdate= CURDATE()',(err,oil_list3)=>{
            if(err){
              res.json(err);
            }
            res.render('oil_list',{
              data:oil_list ,data1:oil_list2,data3:oil_list3,session:req.session
            });
          });
        });
      });
    });
  }
};
controller.check = (req,res) => {
  const {date}=req.body;
  //      res.json(date)
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) =>{
      conn.query('SELECT o.oil_id,o.d1,o.d2,o.uig,o.g95,o.g91,o.e20,o.e85,o.branch_id,o.dp,b.branch_name,b.branch_id,date_format(o.date,"%d/%m/%Y") as date FROM oil_list as o JOIN branch as b on o.branch_id = b.branch_id WHERE date_format(o.date,"%d/%m/%Y") = ?',[date],(err,data)=>{
        conn.query('SELECT o.oil_id,date_format(o.date,"%d/%m/%Y") as date1 FROM oil_list as o GROUP BY o.date DESC',(err,data1)=>{
          conn.query('SELECT c.cdate,c.center_id,b.branch_name,c.cd1,c.cd2,c.cdp,c.cuig,c.cg95,c.cg91,c.ce20,c.ce85,s.sender_name,od.order_name,date_format(c.cdate,"%d/%m/%Y") as cdate FROM oil_list as o JOIN branch as b on o.branch_id = b.branch_id JOIN branch_center as c on c.oil_id = o.oil_id JOIN branch_order as od on c.order_id = od.order_id JOIN sender as s on c.sender_id = s.sender_id WHERE date_format(ADDDATE(c.cdate,-1),"%d/%m/%Y") = ?',[date],(err,data3)=>{
            if(err){
              res.json(err);
            }
            res.render('oil_check',{
              data:data,data1:data1,data3:data3,session:req.session
            });
          });
        });
      });
    });
  }
};
controller.new = (req,res) => {
  const data = null ;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM oil_list',(err,oil_new1)=>{
        conn.query('SELECT * FROM branch',(err,oil_new3)=>{
          conn.query('SELECT o.oil_id,date_format(o.date,"%d/%m/%Y") as date1 FROM oil_list as o GROUP BY o.date DESC',(err,oil_new2)=>{
            res.render('oil_new',{

              data1:oil_new1,data2:oil_new2 ,data3:oil_new3 , session: req.session
            });
          });
        });
      });
    });
  }
};

controller.add = (req,res) => {
  const data = req.body;
  res.json(data)
  if(data.branch_id=="กรุณาเลือก"){
    data.branch_id=null;
  }
  const errors = validationResult(req);
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    if(!errors.isEmpty()){
      req.session.errors = errors;
      req.session.success = false;
      res.redirect('/oil/new');
    }else{
      req.session.success = true;
      req.session.topic = "เพิ่มข้อมูลสำเร็จ";
      req.getConnection((err,conn) =>{
        conn.query('INSERT INTO oil_list set ? ',[data],(err,oil_new) =>{
          console.log(oil_new);
          res.redirect('/oil');
        });
      });
    }
  }
};

controller.edit = (req,res) => {
  const data = req.body;
  const {id} = req.params;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) => {
      conn.query('SELECT o.oil_id,o.d1,o.d2,o.uig,o.g95,o.g91,o.e20,o.e85,o.branch_id,o.dp,b.branch_name,b.branch_id,date_format(o.date,"%Y-%m-%d") as date FROM oil_list as o JOIN branch as b on o.branch_id = b.branch_id where oil_id = ?',[id],(err,data)=>{
        conn.query('SELECT * FROM branch ',(err,data2)=>{
          conn.query('SELECT o.date FROM oil_list as o GROUP BY o.date DESC',(err,data3)=>{


            res.render('oil_edit',{
              data:data , data2:data2 , data3:data3,session: req.session
            });
          });
        });
      });
    });
  }
};

controller.save = (req,res) => {
  const {id} = req.params ;
  const data = req.body;
  const errors = validationResult(req) ;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    if(!errors.isEmpty()){
      req.session.errors = errors;
      req.session.success = false;
      res.redirect('/oil/edit/'+id);
    }else{
      req.session.success = true;
      req.session.topic = "แก้ไขข้อมูลสำเร็จ";
      req.getConnection((err,conn) => {
        conn.query('UPDATE oil_list SET ? WHERE oil_id = ? ',[data,id],(err,checklist_save)=>{
          if (err){
            res.json(err);
          }
          console.log(data);
          res.redirect('/oil/');
        });
      });
    }
  }
}

controller.delete = (req,res) => {
  const {id} = req.params ;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM oil_list as o JOIN branch as b on o.branch_id = b.branch_id where oil_id = ?',[id],(err,data)=>{
        conn.query('SELECT *  FROM oil_list as o GROUP BY o.date DESC',(err,oil_list2)=>{

          if (err){
            res.json(err);
          }
          res.render('oil_delete',{
            data:data , data2:id,session: req.session
          });
        });
      });
    });
  }
};
controller.confirmdelete = (req,res) => {
  const {id}=req.params;
  const errors = validationResult(req);
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    if(!errors.isEmpty()){
      req.session.errors = errors;
      req.session.success = false;
      res.redirect('/oil/delete'+id);
    }else{
      req.getConnection((err,conn) =>{
        conn.query('DELETE FROM oil_list WHERE oil_id=?',[id],(err,checklist)=>{
          if (err){

            req.session.test = "ไม่สามารถลบได้";
            req.session.success = false;

            res.redirect('/oil');
            return;
          }else {
            req.session.success = true;
            req.session.topic = "ลบข้อมูลสำเร็จ";

          }
          console.log(checklist);
          res.redirect('/oil');
        });
      });
    }
  }
};
module.exports = controller;
