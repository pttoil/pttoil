const controller = {};
const { validationResult } = require('express-validator');

controller.list = (req,res) => {
  const data = req.body;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) =>{
      conn.query('SELECT d.id,d.debt,date_format(d.date,"%d/%m/%Y")as date FROM debt as d ORDER BY d.date DESC',(err,debt_list)=>{
        if(err){
          res.json(err);
        }

        res.render('debt_list',{
          data:debt_list,session: req.session
        });
      });
    });
  }
};
controller.new = (req,res) => {
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    res.render('debt_new',{
      session: req.session
    });
  };
};

controller.add = (req,res) => {
  const data = req.body;
  const errors = validationResult(req);
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    if(!errors.isEmpty()){
      req.session.errors = errors;
      req.session.success = false;
      res.redirect('/debt/new');
    }else{
      req.session.success = true;
      req.session.topic = "เพิ่มข้อมูลสำเร็จ";

      req.getConnection((err,conn) =>{
        conn.query('SELECT date_format(debt.date,"%Y-%m-%d")as date FROM debt where debt.date=?',[data.date],(err,data2) =>{
          if(data2.length>0){
            if (data.date == data2[0].date) {
              req.session.test = "ไม่สามารถเพิ่มข้อมูลได้ เนื่องจากมีข้อมูลแล้ว กรุณาลบ หรือ แก้ไข";
              req.session.success = false;
              res.redirect('/debt/new');
            }
          }else if(data2){
            req.getConnection((err,conn) =>{
              conn.query('INSERT INTO debt set ? ',[data],(err,oil_new) =>{
                if (err){
                  res.json(err);
                }
                res.redirect('/debt');
              });
            });
          }
        });
      });
    }
  }
};
controller.delete = (req,res) => {
  const {id} = req.params ;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) => {
      conn.query('SELECT debt.id,debt.debt,date_format(debt.date,"%d-%m-%Y")as date FROM debt where debt.id=?',[id],(err,debt_delete)=>{
        if (err){
          res.json(err);
        }
        res.render('debt_delete',{
          data:debt_delete ,session: req.session
        });
      });
    });
  }
};

controller.confirmdelete = (req,res) => {
  const errors = validationResult(req);
  const {id}=req.params;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    if(!errors.isEmpty()){
      req.session.errors = errors;
      req.session.success = false;
      res.redirect('/debt/delete'+id);
    }else{
      req.getConnection((err,conn) =>{
        conn.query('DELETE FROM debt WHERE id=?',[id],(err,balance_confirmdelete)=>{
          if (err){

            req.session.test = "ไม่สามารถลบได้";
            req.session.success = false;

            res.redirect('/debt');
            return;
          }else {
            req.session.success = true;
            req.session.topic = "ลบข้อมูลสำเร็จ";

          }
          console.log(balance_confirmdelete);
          res.redirect('/debt/');
        });
      });
    }
  }
};

controller.edit = (req,res) => {
  const data = req.body;
  const {id} = req.params;

  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) => {
      conn.query('SELECT debt.id,debt.debt,date_format(debt.date,"%Y-%m-%d")as date FROM debt where debt.id=?',[id],(err,balance_edit)=>{
        res.render('debt_edit',{
          data:balance_edit  , session: req.session
        });
      });
    });
  }
};

controller.save = (req,res) => {
  const {id} = req.params ;
  const data = req.body;
  const errors = validationResult(req) ;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    if (!errors.isEmpty()) {
      req.session.errors = errors;
      req.session.success = false;
      res.redirect('/debt/edit/'+id)
    } else {
      req.session.success = true;
      req.session.topic = "แก้ไขข้อมูลสำเร็จ";
      req.getConnection((err,conn) => {
        conn.query('UPDATE debt SET ? WHERE id = ? ',[data,id],(err,balance_save)=>{
          if (err){
            res.json(err);
          }
          console.log(data);
          res.redirect('/debt/');
        });
      });
    }
  }
}
controller.check = (req,res) => {
  const {date}=req.body;
  const {id} = req.params;
  //res.json(date);
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) =>{
      conn.query('SELECT d.id,d.debt,date_format(d.date,"%d/%m/%Y")as date FROM debt as d where d.date=?',[date],(err,data)=>{
        //  res.json(data);
        if(err){
          res.json(err);
        }
        res.render('debt_check',{
          data:data,session:req.session
          
        });
      });
    });
  }
};



module.exports = controller;
