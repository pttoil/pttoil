const controller = {};
const { validationResult } = require('express-validator');

controller.list = (req,res) => {
  const data = req.body;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM branch_order ',(err,branch_order_list)=>{
        if(err){
          res.json(err);
        }
        res.render('branch_order_list',{
          data:branch_order_list,session: req.session
        });
      });
    });
  }
};

controller.new = (req,res) => {
  const data = null ;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM branch_order ',(err,branch_order_new)=>{
        res.render('branch_order_new',{
          data2:branch_order_new , session: req.session
        });
      });
    });
  }
};

controller.add = (req,res) => {
  const data = req.body;
  const errors = validationResult(req);
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    if(!errors.isEmpty()){
      req.session.errors = errors;
      req.session.success = false;
      res.redirect('/branch_order/new');
    }else{
      req.session.success = true;
      req.session.topic = "เพิ่มข้อมูลสำเร็จ";
      req.getConnection((err,conn) =>{
        conn.query('INSERT INTO branch_order set ?',[data],(err,branch_order_add) =>{
          console.log(branch_order_add);
          res.redirect('/branch_order');
        });
      });
    }
  }
};

controller.delete = (req,res) => {
  const {id} = req.params ;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM branch_order where order_id = ?',[id],(err,branch_order_delete)=>{
        if (err){
          res.json(err);
        }
        res.render('branch_order_delete',{
          data1:branch_order_delete ,session: req.session
        });
      });
    });
  }
};

controller.confirmdelete = (req,res) => {
  const {id}=req.params;
  const errors = validationResult(req);
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    if(!errors.isEmpty()){
      req.session.errors = errors;
      req.session.success = false;
      res.redirect('/branch_order/delete'+id);
    }else{
      req.getConnection((err,conn) =>{
        conn.query('DELETE FROM branch_order WHERE order_id=?',[id],(err,branch_order_confirmdelete)=>{
          if (err){

            req.session.test = "ไม่สามารถลบได้";
            req.session.success = false;

            res.redirect('/branch_order');
            return;
          }else {
            req.session.success = true;
            req.session.topic = "ลบข้อมูลสำเร็จ";

          }
          console.log(branch_order_confirmdelete);
          res.redirect('/branch_order/');
        });
      });
    }
  }
};

controller.edit = (req,res) => {
  const data = req.body;
  const {id} = req.params;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM branch_order where order_id = ?',[id],(err,branch_order_edit)=>{
        res.render('branch_order_edit',{
          data:branch_order_edit  , session: req.session
        });
      });
    });
  }
};

controller.save = (req,res) => {
  const {id} = req.params ;
  const data = req.body;
  const errors = validationResult(req) ;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    if(!errors.isEmpty()){
      req.session.errors = errors;
      req.session.success = false;
      res.redirect('/branch_order/edit/'+id);
    }else{
      req.session.success = true;
      req.session.topic = "แก้ไขข้อมูลสำเร็จ";
      req.getConnection((err,conn) => {
        conn.query('UPDATE branch_order SET ? WHERE order_id = ? ',[data,id],(err,branch_order_save)=>{
          if (err){
            res.json(err);
          }
          console.log(data);
          res.redirect('/branch_order/');
        });
      });
    }}
  }

  module.exports = controller;
