const controller = {};
const { validationResult } = require('express-validator');

controller.list = (req,res) => {
  const data = req.body;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM branch ',(err,branch_list)=>{
        if(err){
          res.json(err);
        }

        res.render('branch_list',{
          data:branch_list,session: req.session
        });
      });
    });
  }
};

controller.new = (req,res) => {
  const data = null ;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM branch ',(err,branch_new)=>{
        res.render('branch_new',{
          data2:branch_new , session: req.session
        });
      });
    });
  }
};

controller.add = (req,res) => {
  const data = req.body;
  const errors = validationResult(req);
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    if(!errors.isEmpty()){
      req.session.errors = errors;
      req.session.success = false;
      res.redirect('/branch/new');
    }else{
      req.session.success = true;
      req.session.topic = "เพิ่มข้อมูลสำเร็จ";
      req.getConnection((err,conn) =>{
        conn.query('INSERT INTO branch set ?',[data],(err,branch_add) =>{
          console.log(branch_add);
          res.redirect('/branch');
        });
      });
    }
  }
};

controller.delete = (req,res) => {
  const {id} = req.params ;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM branch where branch_id = ?',[id],(err,branch_delete)=>{
        if (err){
          res.json(err);
        }
        res.render('branch_delete',{
          data1:branch_delete ,session: req.session
        });
      });
    });
  }
};

controller.confirmdelete = (req,res) => {
  const {id}=req.params;
  const errors = validationResult(req);
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    if(!errors.isEmpty()){
      req.session.errors = errors;
      req.session.success = false;
      res.redirect('/branch/delete'+id);
    }else{
      req.getConnection((err,conn) =>{
        conn.query('DELETE FROM branch WHERE branch_id=?',[id],(err,branch_confirmdelete)=>{
          if (err){

            req.session.test = "ไม่สามารถลบได้";
            req.session.success = false;

            res.redirect('/branch');
            return;
          }else {
            req.session.success = true;
            req.session.topic = "ลบข้อมูลสำเร็จ";

          }
          console.log(branch_confirmdelete);
          res.redirect('/branch/');
        });
      });
    }
  }
};

controller.edit = (req,res) => {
  const data = req.body;
  const {id} = req.params;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM branch where branch_id = ?',[id],(err,branch_edit)=>{
        res.render('branch_edit',{
          data:branch_edit  , session: req.session
        });
      });
    });
  }
};

controller.save = (req,res) => {
  const {id} = req.params ;
  const data = req.body;
  const errors = validationResult(req) ;
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else {
    if(!errors.isEmpty()){
      req.session.errors = errors;
      req.session.success = false;
      res.redirect('/branch/edit/'+id);
    }else{
      req.session.success = true;
      req.session.topic = "แก้ไขข้อมูลสำเร็จ";
      req.getConnection((err,conn) => {
        conn.query('UPDATE branch SET ? WHERE branch_id = ? ',[data,id],(err,branch_save)=>{
          if (err){
            res.json(err);
          }
          console.log(data);
          res.redirect('/branch/');
        });
      });
    }}
  }

  module.exports = controller;
